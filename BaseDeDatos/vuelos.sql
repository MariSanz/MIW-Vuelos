-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 11-01-2017 a las 23:35:29
-- Versión del servidor: 10.1.8-MariaDB
-- Versión de PHP: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `vuelos`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciudad`
--

CREATE TABLE `ciudad` (
  `ID` bigint(20) NOT NULL,
  `CODIGO` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `NOMBRE` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `FRECUENCIA` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `ciudad`
--

INSERT INTO `ciudad` (`ID`, `CODIGO`, `NOMBRE`, `FRECUENCIA`) VALUES
(1, 'OVD', 'Oviedo - España', 1),
(2, 'MAD', 'Madrid - España', 10),
(3, 'BCN', 'Barcelona - España', 1),
(4, 'LCG', 'A coruña - España', 0),
(5, 'ALC', 'Alicante - España', 0),
(8, 'ATH', 'Atenas - Grecia', 0),
(9, 'TXL', 'Berlín - Alemania', 0),
(10, 'BIO', 'Bilbao - España', 1),
(11, 'BRU', 'Bruselas - Bélgica', 0),
(12, 'KRK', 'Cracovia - Polonia', 2),
(13, 'DUB', 'Dublín - Irlanda', 0),
(14, 'GOA', 'Génova - Italia', 3),
(15, 'AMS', 'Ámsterdam - Países Bajos', 0),
(16, 'LIS', 'Lisboa - Portugal', 0),
(17, 'LON', 'Londres - Reino Unido', 0),
(18, 'PAR', 'París - Francia', 0),
(19, 'MXP', 'Milán - Italia', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reserva`
--

CREATE TABLE `reserva` (
  `ID` bigint(20) NOT NULL,
  `COCHE` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `CODIGO` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `APELLIDOS` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `IDENTIFICACION` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `NOMBRE` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `ESTADO` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `MALETAS_GRANDES` int(11) DEFAULT NULL,
  `MALETAS_NORMALES` int(11) DEFAULT NULL,
  `ID_IDA` bigint(20) DEFAULT NULL,
  `ID_USUARIO` bigint(20) DEFAULT NULL,
  `ID_VUELTA` bigint(20) DEFAULT NULL,
  `PLAZAS` int(11) DEFAULT NULL,
  `EMAIL_ASOCIADO` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `reserva`
--

INSERT INTO `reserva` (`ID`, `COCHE`, `CODIGO`, `APELLIDOS`, `IDENTIFICACION`, `NOMBRE`, `ESTADO`, `MALETAS_GRANDES`, `MALETAS_NORMALES`, `ID_IDA`, `ID_USUARIO`, `ID_VUELTA`, `PLAZAS`, `EMAIL_ASOCIADO`) VALUES
(29, 'UTILITARIO', 'QQBCT', 'Sánchez', '12587995N', 'María', 'PENDIENTE_PAGO', 1, 2, 4, 6, 1, NULL, 'luisma_sanchez_22@hotmail.com'),
(30, 'UTILITARIO', '086YL', 'Sánchez', '12587995N', 'María', 'PENDIENTE_PAGO', 1, 2, 4, 6, 1, NULL, 'luisma_sanchez_22@hotmail.com'),
(31, 'UTILITARIO', 'R3GXR', 'Sánchez', '12587995N', 'María', 'PENDIENTE_PAGO', 1, 2, 4, 6, 1, NULL, 'luisma_sanchez_22@hotmail.com'),
(32, 'FURGONETA', 'N8DR0', 'Sánchez', '48879232T', 'María', 'CANCELADA', 0, 0, 112, 7, 113, NULL, 'prueba@prueba.es'),
(33, 'FURGONETA', 'UW9MC', 'Sánchez', '48879232T', 'María', 'CANCELADA', 0, 0, 93, 7, 91, NULL, 'prueba@prueba.es'),
(34, 'FURGONETA', 'ZCMX3', 'Sánchez', 'Z8773326G', 'María', 'PAGADA', 1, 0, 1, 6, 5, NULL, 'luisma_sanchez_22@hotmail.com'),
(35, 'FURGONETA', 'RNSQ3', 'Sánchez', 'Z8773326G', 'María', 'PAGADA', 1, 0, 1, 6, 5, NULL, 'luisma_sanchez_22@hotmail.com'),
(36, 'UTILITARIO', 'DMWJE', 'Sánchez', '45644448M', 'María', 'PAGADA', 0, 0, 113, 6, 111, NULL, 'luisma_sanchez_22@hotmail.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `ID` bigint(20) NOT NULL,
  `CLAVE` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `EMAIL` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `NOMBRE` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`ID`, `CLAVE`, `EMAIL`, `NOMBRE`) VALUES
(6, 'luisma', 'luisma_sanchez_22@hotmail.com', 'Luisma'),
(7, '$2a$10$nAh3vaCvgMzUgrrUFdGkx.byZZFqgBHQagK6tQ5/ktcHQglI8oau6', 'prueba@prueba.es', 'Prueba');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vuelo`
--

CREATE TABLE `vuelo` (
  `ID` bigint(20) NOT NULL,
  `FECHA` datetime DEFAULT NULL,
  `PLAZAS` int(11) DEFAULT NULL,
  `ID_DESTINO` bigint(20) DEFAULT NULL,
  `ID_ORIGEN` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `vuelo`
--

INSERT INTO `vuelo` (`ID`, `FECHA`, `PLAZAS`, `ID_DESTINO`, `ID_ORIGEN`) VALUES
(1, '2017-01-01 09:00:00', 413, 1, 2),
(2, '2017-01-01 12:00:00', 416, 1, 2),
(3, '2017-01-01 20:00:00', 416, 1, 2),
(4, '2017-01-01 10:00:00', 416, 2, 1),
(5, '2017-01-01 17:00:00', 416, 2, 1),
(6, '2017-01-01 09:00:00', 416, 1, 3),
(7, '2017-01-01 13:00:00', 416, 1, 3),
(8, '2017-01-01 16:00:00', 416, 3, 1),
(9, '2017-01-01 21:00:00', 416, 3, 1),
(10, '2017-01-01 08:00:00', 416, 1, 4),
(11, '2017-01-01 15:00:00', 416, 1, 4),
(12, '2017-01-01 21:00:00', 416, 1, 4),
(13, '2017-01-01 10:00:00', 416, 4, 1),
(14, '2017-01-01 14:00:00', 416, 4, 1),
(15, '2017-01-01 19:00:00', 416, 4, 1),
(16, '2017-01-01 22:00:00', 416, 4, 1),
(17, '2017-01-01 10:30:00', 416, 2, 5),
(18, '2017-01-01 14:45:00', 416, 2, 5),
(19, '2017-01-01 19:00:00', 416, 2, 5),
(20, '2017-01-01 09:00:00', 416, 5, 2),
(21, '2017-01-01 18:00:00', 416, 5, 2),
(22, '2017-01-01 08:00:00', 416, 2, 3),
(23, '2017-01-01 12:00:00', 416, 2, 3),
(24, '2017-01-01 17:00:00', 416, 2, 3),
(25, '2017-01-01 07:00:00', 416, 3, 2),
(26, '2017-01-01 10:00:00', 416, 3, 2),
(27, '2017-01-01 16:00:00', 416, 3, 2),
(28, '2017-01-01 09:00:00', 416, 2, 4),
(29, '2017-01-01 13:00:00', 416, 2, 4),
(30, '2017-01-01 17:00:00', 416, 2, 4),
(31, '2017-01-01 10:00:00', 416, 4, 2),
(32, '2017-01-01 15:00:00', 416, 4, 2),
(33, '2017-01-01 18:00:00', 416, 4, 2),
(34, '2017-01-01 12:00:00', 416, 2, 8),
(35, '2017-01-01 16:00:00', 416, 2, 8),
(36, '2017-01-01 20:00:00', 416, 2, 8),
(37, '2017-01-01 16:00:00', 416, 8, 2),
(38, '2017-01-01 19:00:00', 416, 8, 2),
(39, '2017-01-01 09:00:00', 416, 2, 9),
(40, '2017-01-01 13:00:00', 416, 2, 9),
(41, '2017-01-01 17:00:00', 416, 2, 9),
(56, '2017-01-01 11:00:00', 416, 3, 2),
(63, '2017-01-01 19:00:00', 416, 2, 4),
(64, '2017-01-01 11:00:00', 416, 4, 2),
(68, '2017-01-01 19:00:00', 416, 4, 2),
(71, '2017-01-01 11:00:00', 416, 2, 8),
(74, '2017-01-01 17:00:00', 416, 2, 8),
(77, '2017-01-01 21:00:00', 416, 2, 8),
(78, '2017-01-01 18:00:00', 416, 8, 2),
(80, '2017-01-01 15:00:00', 416, 8, 2),
(83, '2017-01-01 08:00:00', 416, 2, 9),
(86, '2017-01-01 11:00:00', 416, 2, 9),
(89, '2017-01-01 16:00:00', 416, 2, 9),
(90, '2017-01-01 09:00:00', 416, 2, 10),
(91, '2017-01-01 15:00:00', 416, 2, 10),
(92, '2017-01-01 15:00:00', 416, 10, 2),
(93, '2017-01-01 20:00:00', 416, 10, 2),
(94, '2017-01-01 13:00:00', 416, 2, 11),
(95, '2017-01-01 18:00:00', 416, 2, 11),
(96, '2017-01-01 08:00:00', 416, 11, 2),
(97, '2017-01-01 12:00:00', 416, 11, 2),
(98, '2017-01-01 19:00:00', 416, 11, 2),
(99, '2017-01-01 10:00:00', 416, 2, 12),
(100, '2017-01-01 18:00:00', 416, 2, 12),
(101, '2017-01-01 11:00:00', 416, 12, 2),
(102, '2017-01-01 16:00:00', 416, 12, 2),
(103, '2017-01-01 20:00:00', 416, 12, 2),
(104, '2017-01-01 10:00:00', 416, 2, 13),
(105, '2017-01-01 16:00:00', 416, 2, 13),
(106, '2017-01-01 20:00:00', 416, 2, 13),
(107, '2017-01-01 09:00:00', 416, 13, 2),
(108, '2017-01-01 15:00:00', 416, 13, 2),
(109, '2017-01-01 19:00:00', 416, 13, 2),
(110, '2017-01-01 06:00:00', 416, 2, 14),
(111, '2017-01-01 10:00:00', 416, 2, 14),
(112, '2017-01-01 17:00:00', 416, 2, 14),
(113, '2017-01-01 10:00:00', 416, 14, 2),
(114, '2017-01-01 16:00:00', 416, 14, 2),
(115, '2017-01-01 21:00:00', 416, 14, 2),
(116, '2017-01-01 11:00:00', 416, 2, 15),
(117, '2017-01-01 17:00:00', 416, 2, 15),
(118, '2017-01-01 16:00:00', 416, 15, 2),
(119, '2017-01-01 20:00:00', 416, 15, 2),
(120, '2017-01-01 10:00:00', 416, 2, 16),
(121, '2017-01-01 18:00:00', 416, 16, 2),
(122, '2017-01-01 12:00:00', 416, 2, 17),
(123, '2017-01-01 19:00:00', 416, 17, 2),
(124, '2017-01-01 09:00:00', 416, 2, 18),
(125, '2017-01-01 17:00:00', 416, 18, 2),
(126, '2017-01-01 10:00:00', 416, 2, 19),
(127, '2017-01-01 18:00:00', 416, 2, 19),
(128, '2017-01-01 14:00:00', 416, 19, 2);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `ciudad`
--
ALTER TABLE `ciudad`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `UKdx83m2ld1r7xqmgk1jtsehiba` (`CODIGO`);

--
-- Indices de la tabla `reserva`
--
ALTER TABLE `reserva`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `UKj11t908mbj50ttavw0jjv5uc5` (`CODIGO`),
  ADD KEY `FKgb3b8qemy2yi83ppoj4cqmauv` (`ID_IDA`),
  ADD KEY `FKant6g7pt3qeoeb4o08qcrooff` (`ID_USUARIO`),
  ADD KEY `FKoi4751wrm5afkpv1tbj73svnc` (`ID_VUELTA`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `UK5kj57sci0og84ngs04ndwa7ch` (`EMAIL`);

--
-- Indices de la tabla `vuelo`
--
ALTER TABLE `vuelo`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `UKoym6bd9lqxylrph0gsyroog14` (`FECHA`,`ID_ORIGEN`,`ID_DESTINO`),
  ADD KEY `FKka5hofw177vd6tg2lqqjtaeuj` (`ID_DESTINO`),
  ADD KEY `FK4rg1h10rw8eiiok94mkfq1bmr` (`ID_ORIGEN`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `ciudad`
--
ALTER TABLE `ciudad`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT de la tabla `reserva`
--
ALTER TABLE `reserva`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `vuelo`
--
ALTER TABLE `vuelo`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=129;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `reserva`
--
ALTER TABLE `reserva`
  ADD CONSTRAINT `FKant6g7pt3qeoeb4o08qcrooff` FOREIGN KEY (`ID_USUARIO`) REFERENCES `usuario` (`ID`),
  ADD CONSTRAINT `FKgb3b8qemy2yi83ppoj4cqmauv` FOREIGN KEY (`ID_IDA`) REFERENCES `vuelo` (`ID`),
  ADD CONSTRAINT `FKoi4751wrm5afkpv1tbj73svnc` FOREIGN KEY (`ID_VUELTA`) REFERENCES `vuelo` (`ID`);

--
-- Filtros para la tabla `vuelo`
--
ALTER TABLE `vuelo`
  ADD CONSTRAINT `FK4rg1h10rw8eiiok94mkfq1bmr` FOREIGN KEY (`ID_ORIGEN`) REFERENCES `ciudad` (`ID`),
  ADD CONSTRAINT `FKka5hofw177vd6tg2lqqjtaeuj` FOREIGN KEY (`ID_DESTINO`) REFERENCES `ciudad` (`ID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
