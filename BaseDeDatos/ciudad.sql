-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 11-01-2017 a las 19:48:38
-- Versión del servidor: 10.1.8-MariaDB
-- Versión de PHP: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `vuelos`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciudad`
--

CREATE TABLE `ciudad` (
  `ID` bigint(20) NOT NULL,
  `CODIGO` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `NOMBRE` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `FRECUENCIA` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `ciudad`
--

INSERT INTO `ciudad` (`ID`, `CODIGO`, `NOMBRE`, `FRECUENCIA`) VALUES
(1, 'OVD', 'Oviedo - España', 1),
(2, 'MAD', 'Madrid - España', 10),
(3, 'BCN', 'Barcelona - España', 1),
(4, 'LCG', 'A coruña - España', 0),
(5, 'ALC', 'Alicante - España', 0),
(8, 'ATH', 'Atenas - Grecia', 0),
(9, 'TXL', 'Berlín - Alemania', 0),
(10, 'BIO', 'Bilbao - España', 1),
(11, 'BRU', 'Bruselas - Bélgica', 0),
(12, 'KRK', 'Cracovia - Polonia', 2),
(13, 'DUB', 'Dublín - Irlanda', 0),
(14, 'GOA', 'Génova - Italia', 3),
(15, 'AMS', 'Ámsterdam - Países Bajos', 0),
(16, 'LIS', 'Lisboa - Portugal', 0),
(17, 'LON', 'Londres - Reino Unido', 0),
(18, 'PAR', 'París - Francia', 0),
(19, 'MXP', 'Milán - Italia', 0);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `ciudad`
--
ALTER TABLE `ciudad`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `UKdx83m2ld1r7xqmgk1jtsehiba` (`CODIGO`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `ciudad`
--
ALTER TABLE `ciudad`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
