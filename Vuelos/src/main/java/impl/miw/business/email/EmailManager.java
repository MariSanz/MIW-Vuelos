package impl.miw.business.email;

import impl.miw.presentation.reservar.ConfirmacionData;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Value;

import com.miw.business.EmailService;
import com.miw.model.Mensaje;
import com.miw.model.Reserva;

public class EmailManager implements EmailService{
	
	private static final String PUERTO ="465";
	
	@Value("${correo.from}")
	private String correoFrom;
	@Value("${correo.clave}")
	private String correoClave;
	
	
    //metodo que recibe y envia el email
    public void enviaEmail(Mensaje msj) {
      
        Properties props = new Properties();//propiedades a agragar
        props.put("mail.smtp.user", msj.getMiCorreo());//correo origen
        props.put("mail.smtp.host", "smtp.gmail.com");//tipo de servidor
        props.put("mail.smtp.port", PUERTO);//puesto de salida
        props.put("mail.smtp.starttls.enable", "true");//inicializar el servidor
        props.put("mail.smtp.auth", "true");//autentificacion
        props.put("mail.smtp.socketFactory.port", PUERTO);//activar el puerto
        props.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.socketFactory.fallback", "false");
        //SecurityManager security = System.getSecurityManager();
        try {
            Authenticator auth = new autentificadorSMTP(msj);//autentificar el correo
            Session session = Session.getInstance(props, auth);//se inica una session
            // session.setDebug(true);
            MimeMessage msg = new MimeMessage(session);//se crea un objeto donde ira la estructura del correo
            msg.setText(msj.getCuerpo());//seteo el cuertpo del mensaje
            msg.setSubject(msj.getAsunto());//setea asusto (opcional)
            msg.setFrom(new InternetAddress(msj.getMiCorreo()));//agrega la la propiedad del correo origen
            msg.addRecipient(Message.RecipientType.TO, new InternetAddress(msj.getMailReceptor()));//agrega el destinatario
            Transport.send(msg);//envia el mensaje
        } catch (Exception mex) {//en caso de que ocurra un error se crea una excepcion
        	throw new RuntimeException();
        }//fin try-catch
    }//fin metodo enviaEmail
    
    
    private class autentificadorSMTP extends javax.mail.Authenticator {
    	
    	private Mensaje msj;
    	
    	public autentificadorSMTP(Mensaje mensaje) {
			this.msj = mensaje;
		}
    	
        @Override
        public PasswordAuthentication getPasswordAuthentication() {
            return new PasswordAuthentication(msj.getMiCorreo(), msj.getMiContrase�a());//autentifica tanto el correo como la contrase�a
        }
    }

	@Override
	public void enviarConfirmacionReserva(ConfirmacionData datos) {
		Mensaje msj = new Mensaje();
		
		msj.setMailReceptor(datos.getCorreo());
		msj.setAsunto("Confirmacion de reserva en VuelosToday "+datos.getReserva().getCodigo());
		msj.setMiCorreo(correoFrom);
		msj.setMiContrase�a(correoClave);
		StringBuilder sb = new StringBuilder();
		sb.append("Nos es grato notificarme la confirmaci�n de su reserva con c�dgo ")
			.append(datos.getReserva().getCodigo())
			.append("\n");
		sb.append("-------------------Datos del vuelo-----------------");
		sb.append("\nVuelo Ida \n")
			.append("Fecha: ").append(datos.getReserva().getFechaIda())
			.append("\t ").append(datos.getReserva().getVueloIda().getFecha())
			.append("\n \t Ciudad: ").append(datos.getReserva().getVueloIda().getOrigen().getNombre())
			.append("\n");
		if(datos.getReserva().getVueloVuelta()!=null){
			sb.append("\nVuelo Vuelta \n")
			.append("Fecha: ").append(datos.getReserva().getFechaVuelta())
			.append("\t ").append(datos.getReserva().getVueloVuelta().getFecha())
			.append("\n \t Ciudad: ").append(datos.getReserva().getVueloVuelta().getOrigen().getNombre());
		}
		
		sb.append("Maletas normales: ")
			.append(datos.getReserva().getNumMaletaNormal())
			.append("\n");
		sb.append("Maletas grandes: ")
			.append(datos.getReserva().getNumMaletaGrande())
			.append("\n");
		
		sb.append("Coche: ")
			.append(datos.getReserva().getCoche().toString())
			.append("\n");
		
		sb.append("Estado reserva: ")
			.append(datos.getEstadoReserva().toString())
			.append("\n");
		
		sb.append("-------------------Datos reserva-----------------")
			.append("\n");
		
		sb.append("Nombre y apellidos: ")
			.append(datos.getNombre())
			.append(" \n"+datos.getApellidos())
			.append("\n");
		sb.append("Identificaci�n: ")
			.append(datos.getIdentificacion())
			.append("\n");
		sb.append("Correo asociado a la reserva: ")
			.append(datos.getCorreo())
			.append("\n");
		
		msj.setCuerpo(sb.toString());
		
		enviaEmail(msj);
		
	}

	@Override
	public void enviarCancelacionReserva(Reserva r) {
	Mensaje msj = new Mensaje();
		
		msj.setMailReceptor(r.getEmailAsocido());
		msj.setAsunto("Confirmacion de CANCELACI�N reserva en VuelosToday "+r.getCodigo());
		msj.setMiCorreo(correoFrom);
		msj.setMiContrase�a(correoClave);
		StringBuilder sb = new StringBuilder();
		sb.append("Usted ha cancelado la reserva")
			.append("\n");
		
		
		sb.append("Estado reserva: ")
			.append(r.getEstado())
			.append(" c�digo ")
			.append(r.getCodigo())
			.append("\n");
		
		sb.append("-------------------Datos reserva-----------------")
			.append("\n");
		
		sb.append("Nombre y apellidos: ")
			.append(r.getDatosDeCobro().getNombre())
			.append(" \n"+r.getDatosDeCobro().getApellidos())
			.append("\n");
		sb.append("Identificaci�n: ")
			.append(r.getDatosDeCobro().getIdentificacion())
			.append("\n");
		sb.append("Correo asociado a la reserva: ")
			.append(r.getEmailAsocido())
			.append("\n");
		
		msj.setCuerpo(sb.toString());
		
		enviaEmail(msj);
		
	}

}
