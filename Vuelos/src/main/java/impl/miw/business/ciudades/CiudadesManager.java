package impl.miw.business.ciudades;

import impl.miw.presentation.buscador.CiudadData;
import impl.miw.presentation.buscador.FrecuenciaData;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.miw.business.CiudadesService;
import com.miw.model.Ciudad;
import com.miw.model.Vuelo;
import com.miw.persistence.CiudadesDataService;
import com.miw.persistence.VuelosDataService;

public class CiudadesManager implements CiudadesService {

	@Autowired
	private CiudadesDataService ciudadesDAO;
	
	@Autowired
	private VuelosDataService vuelosDAO;
	
	
	@Override
	public List<Ciudad> buscarPorNombre(String nombre) {
		return ciudadesDAO.buscarPorNombre(nombre);
	}

	@Override
	public List<Ciudad> buscarCiudadVueloOrigen(String match) {
		List<Ciudad> ciudadOrigen = ciudadesDAO.buscarPorNombre(match);	
		
		return ciudadOrigen;
	}

	@Override
	public List<Ciudad> buscarCiudadVueloDestino(Long origen, String match) {
		List<Vuelo> vuelosOrigen = vuelosDAO.buscarPorDestino(origen, match);
		
		List<Ciudad> ciudades = new ArrayList<Ciudad>();
		for(Vuelo v: vuelosOrigen){
			ciudades.add(v.getDestino());
		}
		return ciudades;
	}

	@Override
	public void actualizarFrecuencia(Long ciudadId) {
		
		ciudadesDAO.actualizarFrecuencia(ciudadId);
		
	}

	@Override
	public FrecuenciaData buscarDestinosFrecuentes() {
		List<Ciudad> destinos = ciudadesDAO.destinosFrecuentes();
		//total de vistas
		Long vistas = ciudadesDAO.buscarVistas();
		int total = ciudadesDAO.buscarTotalCiudades().intValue();
		CiudadData cd= new CiudadData();
		List<CiudadData> listCd = new ArrayList<CiudadData>();
		for(Ciudad c: destinos){
			cd = new CiudadData();
			cd.setDestino(c);
			cd.setVistas(vistas.intValue());
			cd.setFrecuencia(((c.getFrecuencia() *100)/(vistas.intValue())));
			listCd.add(cd);
			
		}
		FrecuenciaData fData = new FrecuenciaData();
		fData.setDestinos(listCd);
		fData.setTotalCiudades(total);
		fData.setVistas(vistas.intValue());
		
		
		return fData;
	}
	
	

}
