package impl.miw.business.vuelo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.miw.business.VuelosService;
import com.miw.model.Vuelo;
import com.miw.persistence.VuelosDataService;

public class VuelosManager implements VuelosService{
	
	@Autowired
	private VuelosDataService vuelosDataService;	
	
	@Override
	public List<Vuelo> buscarPorDestino(Long origen, String match) {
		
		return vuelosDataService.buscarPorDestino(origen, match);
	}

	@Override
	public List<Vuelo> buscarVuelos(Long origen, Long destino) {
		
		return vuelosDataService.buscarPorTrayecto(origen, destino);
	}

	@Override
	public Vuelo buscarPorId(Long idVueloIda) {
		
		return vuelosDataService.buscarPorId(idVueloIda);
	}

	
}
