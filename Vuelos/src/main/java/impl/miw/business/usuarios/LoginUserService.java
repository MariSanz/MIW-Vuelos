package impl.miw.business.usuarios;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.miw.model.Usuario;
import com.miw.persistence.UsuariosDataService;

public class LoginUserService implements UserDetailsService{
	
	@Autowired
	private UsuariosDataService usuariosDataService;

	@Override
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {
		
		Usuario usuario = usuariosDataService.buscarPorEmail(username);
		if (usuario == null) {
			throw new UsernameNotFoundException(username);
		}
		
		return new User(usuario.getEmail(), usuario.getClave(), Arrays.asList(new SimpleGrantedAuthority("ROLE_CONSULTANT")));
	}
	
	

}
