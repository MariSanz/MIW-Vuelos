package impl.miw.business.usuarios;

import impl.miw.presentation.login.RegistroData;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.miw.business.UsuariosService;
import com.miw.model.Usuario;
import com.miw.persistence.UsuariosDataService;

public class UsuariosManager implements UsuariosService{
	
	@Autowired
	private UsuariosDataService usuariosDataService;

	@Override
	public void registrar(RegistroData data) {
		Usuario usuario = data.toUsuario();

		if (usuariosDataService.buscarPorEmail(usuario.getEmail()) != null) {
			throw new RuntimeException("Usuario ya existe");
		}
		
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		String clave = passwordEncoder.encode(usuario.getClave());
		usuario.setClave(clave);
		
		usuariosDataService.insertar(usuario);
		
	}

	

}
