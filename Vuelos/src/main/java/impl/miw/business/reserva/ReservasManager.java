package impl.miw.business.reserva;

import impl.miw.presentation.historial.HistorialData;
import impl.miw.presentation.reservar.ConfirmacionData;
import impl.miw.presentation.reservar.GuardarData;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.miw.business.EmailService;
import com.miw.business.ReservasService;
import com.miw.model.DatosDeCobro;
import com.miw.model.EstadoReserva;
import com.miw.model.Reserva;
import com.miw.model.Usuario;
import com.miw.model.Vuelo;
import com.miw.persistence.ReservasDataService;
import com.miw.persistence.UsuariosDataService;
import com.miw.persistence.VuelosDataService;

public class ReservasManager implements ReservasService{

	@Autowired
	private ReservasDataService reservasData;
	
	@Autowired
	private EmailService emailService;
	
	@Autowired
	private UsuariosDataService usuariosData;
	
	@Autowired
	private VuelosDataService vuelosData;
	
	public static final Double PRECIO_RESERVA = 15.00;
	
	@Override
	public GuardarData guardar(ConfirmacionData data) {
		
		GuardarData gData = new GuardarData();
		
		Reserva reserva;
		do {
			reserva = Reserva.generar();
		}
		while (reservasData.buscarPorCodigo(reserva.getCodigo()) != null);
		
		Vuelo vueloIda = vuelosData.buscarPorId(data.getReserva().getIdVueloIda());
		gData.setPrecioReserva(PRECIO_RESERVA);
		Vuelo vueloVuelta = vuelosData.buscarPorId(data.getReserva().getIdVueloVuelta());
		
		reserva.setIda(vueloIda);
		if(vueloVuelta != null){
			reserva.setVuelta(vueloVuelta);
			gData.setPrecioReserva(PRECIO_RESERVA*2);			
		}
		
		reserva.setMaletasGrandes(data.getReserva().getNumMaletaGrande());
		reserva.setMaletasPequenas(data.getReserva().getNumMaletaNormal());
		reserva.setEmailAsocido(data.getCorreo());
		
		//Pago reserva
		reserva.setEstado(data.getEstadoReserva());
		
		//Tipo de coche
		reserva.setCoche(data.getTipoCoche());
		
		//Datos de usuario
		DatosDeCobro datos = new DatosDeCobro();
		datos.setNombre(data.getNombre());
		datos.setApellidos(data.getApellidos());
		datos.setIdentificacion(data.getIdentificacion());
		reserva.setDatosDeCobro(datos);
		
		Usuario u = usuariosData.buscarPorEmail(data.getCorreo());
		if(u!=null){
			reserva.setUsuario(u);
		}
		
		

		reservasData.insertar(reserva);		
		
		gData.setCodigoReserva(reserva.getCodigo());
		
		return gData;
	}

	@Override
	public void actualizarEstadoPago(String codReserva) {
		Reserva update = reservasData.buscarPorCodigo(codReserva);
		
		update.setEstado(EstadoReserva.PAGADA);
		
		reservasData.insertar(update);
		
	}

	@Override
	public Collection<HistorialData> reservasUsuario(String usuario) {
		List<Reserva> reservasUsuario = (List<Reserva>) reservasData.buscarReservasPorUsuario(usuario);
		List<HistorialData> datas = new ArrayList<HistorialData>();
		
		for(Reserva rs : reservasUsuario){
			HistorialData historial = new HistorialData();
			BeanUtils.copyProperties(rs, historial);
			historial.setIdentificacion(rs.getDatosDeCobro().getIdentificacion());
			datas.add(historial);
		}
		return datas;
	}

	@Override
	public void cancelarReserva(String codigo, String email) {
		Reserva r =reservasData.buscarPorCodigo(codigo);
		
		if(r.getEmailAsocido().equals(email)){
			r.setEstado(EstadoReserva.CANCELADA);
			//se actualiza el estado en la bd
			reservasData.insertar(r);
			emailService.enviarCancelacionReserva(r);
		}else{
			throw new RuntimeException();
		}
		
	}

	@Override
	public Reserva buscarPorCodigo(String codigoReserva) {
		
		return reservasData.buscarPorCodigo(codigoReserva);
	}
}
