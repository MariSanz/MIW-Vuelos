package impl.miw.persistence.reserva;

import java.util.Collection;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.miw.model.Reserva;
import com.miw.persistence.ReservasDataService;

public class ReservasDAO implements ReservasDataService {

	@PersistenceContext
	private EntityManager em;
	
	@Override
	public void insertar(Reserva reserva) {
		em.persist(reserva);
	}
	
	@Override
	public Reserva buscarPorCodigo(String codigo) {
		List<Reserva> reserva = em.createQuery("SELECT r FROM Reserva r WHERE r.codigo = :codigo", Reserva.class)
				.setParameter("codigo", codigo)
				.getResultList();
		
		return reserva.isEmpty() ? null : reserva.get(0);
	}

	@Override
	public Collection<Reserva> buscarReservasPorUsuario(String usuario) {
		List<Reserva> reservas = em.createQuery("SELECT r FROM Reserva r WHERE r.emailAsocido = :email", Reserva.class)
				.setParameter("email", usuario)
				.getResultList();
		
		return reservas;
	}
}
