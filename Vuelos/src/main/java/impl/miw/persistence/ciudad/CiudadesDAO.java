package impl.miw.persistence.ciudad;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.miw.model.Ciudad;
import com.miw.persistence.CiudadesDataService;

public class CiudadesDAO implements CiudadesDataService {
	
	@PersistenceContext
	private EntityManager em;

	@Override
	public List<Ciudad> buscarPorNombre(String nombre) {
		return em.createQuery("SELECT c FROM Ciudad c WHERE c.nombre LIKE :ciudad", Ciudad.class)
				.setParameter("ciudad", "%" + nombre + "%")
				.getResultList();
	}

	@Override
	public void actualizarFrecuencia(Long ciudadId) {
		Ciudad ciudad = buscarPorId(ciudadId);
		int freActual = ciudad.getFrecuencia()+1;
		ciudad.setFrecuencia(freActual);
		
		em.persist(ciudad);
		
	}

	@Override
	public Ciudad buscarPorId(Long id) {
		return em.createQuery("SELECT c FROM Ciudad c WHERE c.id=:ident", Ciudad.class)
		.setParameter("ident", id)
		.getSingleResult();
	}

	@Override
	public List<Ciudad> destinosFrecuentes() {
		return em.createQuery("SELECT c FROM Ciudad c WHERE 1=1 ORDER BY c.frecuencia DESC", Ciudad.class)
				.setMaxResults(5)
				.getResultList();
	}

	@Override
	public Long buscarVistas() {
		return em.createQuery("SELECT SUM(c.frecuencia) FROM Ciudad c WHERE 1=1", Long.class)
				.getSingleResult();
	}

	@Override
	public Long buscarTotalCiudades() {
		
		return em.createQuery("SELECT COUNT(c.codigo) FROM Ciudad c WHERE 1=1", Long.class)
				.getSingleResult();
	}

}
