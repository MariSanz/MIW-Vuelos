package impl.miw.persistence.vuelo;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.miw.model.Vuelo;
import com.miw.persistence.VuelosDataService;

public class VuelosDAO implements VuelosDataService {
	
	@PersistenceContext
	private EntityManager em;


	@Override
	public List<Vuelo> buscarPorDestino(Long origen, String match) {
				
		return em.createQuery("SELECT v FROM Vuelo v WHERE v.origen.id =:idOrigen and v.destino.nombre LIKE :ciudad group by v.destino.codigo", Vuelo.class)
				.setParameter("idOrigen", origen)
				.setParameter("ciudad",  "%" +match + "%" )
				.getResultList();
	}


	@Override
	public List<Vuelo> buscarPorTrayecto(Long origen, Long destino) {
		
		return em.createQuery("SELECT v FROM Vuelo v WHERE v.origen.id =:idOrigen and v.destino.id =:idDestino", Vuelo.class)
				.setParameter("idOrigen", origen)
				.setParameter("idDestino",  destino)
				.getResultList();
	}


	@Override
	public Vuelo buscarPorId(Long idVuelo) {
		
		return em.createQuery("SELECT v FROM Vuelo v WHERE v.id=:idVuelo", Vuelo.class)
				.setParameter("idVuelo", idVuelo)
				.getSingleResult();
	}


	@Override
	public List<Vuelo> buscarPorTrayectoAndPlazas(Long origen, Long destino,
			Integer plazas) {
		return em.createQuery("SELECT v FROM Vuelo v WHERE v.origen.id =:idOrigen and v.destino.id =:idDestino and v.plazas>=:numPlazas", Vuelo.class)
				.setParameter("idOrigen", origen)
				.setParameter("idDestino",  destino)
				.setParameter("numPlazas", plazas)
				.getResultList();
	}

}
