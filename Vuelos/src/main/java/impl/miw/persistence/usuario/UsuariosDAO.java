package impl.miw.persistence.usuario;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.miw.model.Usuario;
import com.miw.persistence.UsuariosDataService;

public class UsuariosDAO implements UsuariosDataService {

	@PersistenceContext
	private EntityManager em;
	
	@Override
	public Usuario buscarPorEmail(String email) {
		List<Usuario> usuario = em.createQuery("SELECT u FROM Usuario u WHERE u.email = :email", Usuario.class)
				.setParameter("email", email)
				.getResultList();
		
		return usuario.isEmpty() ? null : usuario.get(0);
	}

	@Override
	public void insertar(Usuario usuario) {
	
		
			
		em.persist(usuario);
		
		
	}
}
