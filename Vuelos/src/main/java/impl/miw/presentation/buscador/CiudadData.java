package impl.miw.presentation.buscador;

import com.miw.model.Ciudad;

public class CiudadData {
	
	private Ciudad destino;
	private int frecuencia;
	private int vistas;
	
	public Ciudad getDestino() {
		return destino;
	}
	
	public void setDestino(Ciudad destino) {
		this.destino = destino;
	}
	
	public int getFrecuencia() {
		return frecuencia;
	}
	
	public void setFrecuencia(int frecuencia) {
		this.frecuencia = frecuencia;
	}
	
	public int getVistas() {
		return vistas;
	}
	
	public void setVistas(int vistas) {
		this.vistas = vistas;
	}
	

}
