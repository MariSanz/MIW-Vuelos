package impl.miw.presentation.buscador;

import java.util.ArrayList;
import java.util.List;

import com.miw.model.Vuelo;


public class ResultadoData {
	
	private String ciudadIda;
	private String fechaIda;
	private String ciudadVuelta;
	private String fechaVuelta;
	private List<Vuelo> vuelosIda = new ArrayList<Vuelo>();
	private List<Vuelo> vuelosVuelta = new ArrayList<Vuelo>();
	private int plazas;
	
	public String getCiudadIda() {
		return ciudadIda;
	}
	
	public void setCiudadIda(String ciudadIda) {
		this.ciudadIda = ciudadIda;
	}
	
	public String getFechaIda() {
		return fechaIda;
	}
	
	public void setFechaIda(String fechaIda) {
		this.fechaIda = fechaIda;
	}
	
	public String getCiudadVuelta() {
		return ciudadVuelta;
	}
	
	public void setCiudadVuelta(String ciudadVuelta) {
		this.ciudadVuelta = ciudadVuelta;
	}
	
	public String getFechaVuelta() {
		return fechaVuelta;
	}
	
	public void setFechaVuelta(String fechaVuelta) {
		this.fechaVuelta = fechaVuelta;
	}
	
	public int getPlazas() {
		return plazas;
	}
	
	public void setPlazas(int plazas) {
		this.plazas = plazas;
	}
	
	public List<Vuelo> getVuelosIda() {
		return vuelosIda;
	}
	
	public void setVuelosIda(List<Vuelo> vuelosIda) {
		this.vuelosIda = vuelosIda;
	}
	
	public List<Vuelo> getVuelosVuelta() {
		return vuelosVuelta;
	}
	
	public void setVuelosVuelta(List<Vuelo> vuelosVuelta) {
		this.vuelosVuelta = vuelosVuelta;
	}
	
	
	
	
}
