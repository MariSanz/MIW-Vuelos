package impl.miw.presentation.buscador;

import java.util.List;

public class FrecuenciaData {

		
		private List<CiudadData> destinos;
		private int totalCiudades;
		private int vistas;
		
		public List<CiudadData> getDestinos() {
			return destinos;
		}
		
		public void setDestinos(List<CiudadData> destinos) {
			this.destinos = destinos;
		}
		
		public int getVistas() {
			return vistas;
		}
		
		public void setVistas(int vistas) {
			this.vistas = vistas;
		}
		
		public int getTotalCiudades() {
			return totalCiudades;
		}
		
		public void setTotalCiudades(int totalCiudades) {
			this.totalCiudades = totalCiudades;
		}



}
