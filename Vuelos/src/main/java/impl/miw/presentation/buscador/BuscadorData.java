package impl.miw.presentation.buscador;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;


public class BuscadorData {
	
	@NotNull(message = "{buscador.notOrigen}")
	private Long origen;
	@NotNull(message = "{buscador.notDestino}")
	private Long destino;
	@NotNull
	private Boolean idaVuelta;
	@NotEmpty(message = "{buscador.notFechaIda}")
	private String fechaIda;
	private String fechaVuelta;
		
	public Long getOrigen() {
		return origen;
	}
	
	public void setOrigen(Long origen) {
		this.origen = origen;
	}
	
	public Long getDestino() {
		return destino;
	}
	
	public void setDestino(Long destino) {
		this.destino = destino;
	}
	
	public Boolean getIdaVuelta() {
		return idaVuelta;
	}
	
	public void setIdaVuelta(Boolean idaVuelta) {
		this.idaVuelta = idaVuelta;
	}
	
	public String getFechaIda() {
		return fechaIda;
	}
	
	public void setFechaIda(String fechaIda) {
		this.fechaIda = fechaIda;
	}
	
	public String getFechaVuelta() {
		return fechaVuelta;
	}
	
	public void setFechaVuelta(String fechaVuelta) {
		this.fechaVuelta = fechaVuelta;
	}

	@Override
	public String toString() {
		return "BuscadorData [origen=" + origen + ", destino=" + destino
				+ ", idaVuelta=" + idaVuelta + ", fechaIda=" + fechaIda
				+ ", fechaVuelta=" + fechaVuelta + "]";
	}
	
	
}
