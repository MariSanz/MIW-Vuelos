package impl.miw.presentation.buscador;

import impl.miw.presentation.imprimir.ImprimirData;
import impl.miw.presentation.reservar.ReservaData;

import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.miw.business.CiudadesService;
import com.miw.business.VuelosService;
import com.miw.model.Ciudad;
import com.miw.model.Vuelo;

@Controller
@RequestMapping("/")
public class BuscadorController {
	
	@Autowired
	private CiudadesService ciudadesService;
	
	@Autowired
	private VuelosService vuelosService;
	
	@RequestMapping(method = RequestMethod.GET)
	public String get(Model model)
	{
		FrecuenciaData destinos =  ciudadesService.buscarDestinosFrecuentes();
		
		model.addAttribute("destinosFrecuentes", destinos);
		model.addAttribute("buscadorData",new BuscadorData());		
		return "index";
	}
	
	@RequestMapping(value="/ciudadesOrigen", method = RequestMethod.GET)
	@ResponseBody
	public List<Ciudad> getOrigen(@RequestParam("q") String match,  Model model)
	{
		return ciudadesService.buscarPorNombre(match);
	}
	
	@RequestMapping(value="/ciudadesDestino", method = RequestMethod.GET)
	@ResponseBody
	public List<Ciudad> getDestino(@RequestParam("o") Long origen, @RequestParam("q") String match, Model model)
	{
		return ciudadesService.buscarCiudadVueloDestino(origen, match);
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public String post(@Valid @ModelAttribute BuscadorData data, BindingResult result, Model model, HttpSession sesion)
	{
		if ( result.hasErrors())
		{
			model.addAttribute("error", "buscador.errorFormulario");
			return "index";
		}
		try{
			
			if(data.getIdaVuelta() && data.getFechaVuelta().isEmpty()){
				model.addAttribute("error", "buscador.errorVuelta");
				return "index";
			}
			ResultadoData rData= new ResultadoData();
			
			List<Vuelo> ida =this.vuelosService.buscarVuelos(data.getOrigen(), data.getDestino());
			ciudadesService.actualizarFrecuencia(data.getDestino());
			
			rData.setVuelosIda(ida);
			
			rData.setCiudadIda(ida.get(0).getOrigen().getNombre());
			rData.setFechaIda(data.getFechaIda());
			
			if(data.getIdaVuelta()){
				List<Vuelo> vuelta = this.vuelosService.buscarVuelos(data.getDestino(), data.getOrigen());
				rData.setVuelosVuelta(vuelta);

				rData.setCiudadVuelta(ida.get(0).getDestino().getNombre());
				rData.setFechaVuelta(data.getFechaVuelta());
			}
			
			model.addAttribute("resultadosData", rData);
			ReservaData reservaData = new ReservaData();
			reservaData.setFechaIda(rData.getFechaIda());
			reservaData.setFechaVuelta(rData.getFechaVuelta());
			model.addAttribute("reservarData", reservaData);
			
			/*Mantengo en sesi�n los datos para imprimir resultados de busqueda*/
			ImprimirData imData = new ImprimirData();
			imData.setResultadoBusqueda(rData);
			sesion.setAttribute("imprimirResultados", imData);
			
			
			return "resultados";
			
		}catch(Exception e){
			return "error";
		}
		
		
	}


}
