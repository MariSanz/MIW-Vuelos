package impl.miw.presentation.imprimir;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.UUID;

import com.itextpdf.text.Document;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.ZapfDingbatsList;
import com.itextpdf.text.pdf.CMYKColor;
import com.itextpdf.text.pdf.PdfWriter;
import com.miw.model.Vuelo;

public class CreatePDF {
	
	private static final Font fontPrincipal = FontFactory.getFont(FontFactory.HELVETICA, 14, Font.BOLD, new CMYKColor(10,10,10,100));;
	private static final Font fontTitulo = FontFactory.getFont(FontFactory.COURIER, 20, Font.BOLD,new CMYKColor(0, 95, 100, 0));;
	
	private String rutaTemporalSalida;
	
	public void setRutaTemporalSalida(String rutaTemporalSalida) {
		this.rutaTemporalSalida = rutaTemporalSalida;
	}
	
	public File createResultadosBusqueda(ImprimirData data) {
		
		if (rutaTemporalSalida == null) {
			throw new RuntimeException("No se indicaron las rutas");
		}
		
		try
	      {
			UUID idFichero = UUID.randomUUID();
			
			File pdf = new File(rutaTemporalSalida + idFichero.toString() + ".pdf");
			 Document document = new Document();
	         PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(pdf));
	         document.open();
	        
	         /*A�adir imagen logo*/
	         //Add Image
	         URL logo = getClass().getClassLoader().getResource("/logo_md.png");
	         Image image1 = Image.getInstance(logo);
	         //Fixed Positioning
	         image1.setAbsolutePosition(30f, 700f);
	         //Scale to new height and new width of image
	         image1.scaleAbsolute(100, 100);
	         //Add to document
	         document.add(image1);
	         document.add(new Paragraph("\n \n"));
	         document.add(new Paragraph("\t \t \t \t Resultados de la busqueda de vuelos", fontTitulo));
	         document.add(new Paragraph("\n \n"));
	         
	         document.add(new Paragraph("IDA \t"
	        		 +" "+ data.getResultadoBusqueda().getFechaIda() 
	        		 + "\t Desde "
	        		 +data.getResultadoBusqueda().getCiudadIda()
	        		 + "\t a \t"
	        		 +data.getResultadoBusqueda().getCiudadVuelta(), fontPrincipal));
	         document.add(new Paragraph("\n"));
	         
	         //ZapfDingbatsList List Example
	         DateFormat fechaHora = new SimpleDateFormat("HH:mm");
	         ZapfDingbatsList zapfDingbatsList = new ZapfDingbatsList(83, 20);
	         
	         for(Vuelo vIda : data.getResultadoBusqueda().getVuelosIda()){	        	 
	     		String fecha = fechaHora.format(vIda.getFecha());
	        	zapfDingbatsList.add(new ListItem(fecha));
	         }
	       
	         document.add(zapfDingbatsList);
	         document.add(new Paragraph("\n"));
	         
	         if(!data.getResultadoBusqueda().getVuelosVuelta().isEmpty()){
	        	 
	        	 document.add(new Paragraph("VUELTA \t"
		        		 +" "+ data.getResultadoBusqueda().getFechaVuelta()
		        		 + "\t Desde "
		        		 +data.getResultadoBusqueda().getCiudadVuelta()
		        		 + "\t a \t"
		        		 +data.getResultadoBusqueda().getCiudadIda(), fontPrincipal));
		         document.add(new Paragraph("\n"));
		         
		         zapfDingbatsList = new ZapfDingbatsList(83, 20);
		         for(Vuelo vVuelta : data.getResultadoBusqueda().getVuelosVuelta()){	        	 
			     		String fecha = fechaHora.format(vVuelta.getFecha());
			        	zapfDingbatsList.add(new ListItem(fecha));
			      }
			       
			      document.add(zapfDingbatsList);
	         }
	         
	         document.close();
	         writer.close();
	         
	         return pdf;
	      } catch (Exception e) {
	    	  throw new RuntimeException(e);
	      }
	}

	public File createReservaConfirmada(ImprimirData data) {
		if (rutaTemporalSalida == null) {
			throw new RuntimeException("No se indicaron las rutas");
		}
		
		try
	      {
			UUID idFichero = UUID.randomUUID();
			
			Document document = new Document();
			File pdf = new File(rutaTemporalSalida + idFichero.toString() + ".pdf");
			
	         PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(pdf));
	         document.open();
	        
	         /*A�adir imagen logo*/
	         //Add Image
	         URL logo = getClass().getClassLoader().getResource("/logo_md.png");
	         Image image1 = Image.getInstance(logo);
	         //Fixed Positioning
	         image1.setAbsolutePosition(30f, 700f);
	         //Scale to new height and new width of image
	         image1.scaleAbsolute(100, 100);
	         //Add to document
	         document.add(image1);
	         document.add(new Paragraph("\n \n"));
	         document.add(new Paragraph("\t \t \t \t Datos de su reserva", fontTitulo));
	         document.add(new Paragraph("\n \n"));
	         
	         document.add(new Paragraph("C�digo de reserva:", fontPrincipal));
	         document.add(new Paragraph("\t "+ data.getConfirmarData().getReserva().getCodigo()));
	         
	         document.add(new Paragraph("Vuelo ida:", fontPrincipal));
	         DateFormat formatoHora = new SimpleDateFormat("HH:mm");
	         document.add(new Paragraph("\t "+ data.getConfirmarData().getReserva().getFechaIda()
	        		 			+" - "+ formatoHora.format(data.getConfirmarData().getReserva().getVueloIda().getFecha())));
	         document.add(new Paragraph("\t "+ data.getConfirmarData().getReserva().getVueloIda().getOrigen().getNombre()));
	         
	         if(data.getConfirmarData().getReserva().getVueloVuelta() != null){
	        	 document.add(new Paragraph("Vuelo vuelta:", fontPrincipal));
		         document.add(new Paragraph("\t "+ data.getConfirmarData().getReserva().getFechaVuelta()
		        		 			+ " - "+ formatoHora.format(data.getConfirmarData().getReserva().getVueloVuelta().getFecha())));
		         document.add(new Paragraph("\t "+ data.getConfirmarData().getReserva().getVueloVuelta().getOrigen().getNombre()));
	         }
	        
	         document.add(new Paragraph("Maletas normales:", fontPrincipal));
	         document.add(new Paragraph("\t "+ data.getConfirmarData().getReserva().getNumMaletaNormal()));
	         document.add(new Paragraph("Maletas grandes:", fontPrincipal));
	         document.add(new Paragraph("\t "+ data.getConfirmarData().getReserva().getNumMaletaGrande()));
	         document.add(new Paragraph("Coche de alquiler:", fontPrincipal));
	         switch (data.getConfirmarData().getReserva().getCoche()) {
			case 1:
				document.add(new Paragraph("\t "+ "Utilitario")); 
				break;
			case 2:
				document.add(new Paragraph("\t Furgoneta")); 
				break;

			default:
				document.add(new Paragraph("\t Ninguno"));
				break;
			}
	         
	         document.add(new Paragraph("Estado de reserva:", fontPrincipal));
	         if(data.getConfirmarData().getPagarReserva()) {
	        	 document.add(new Paragraph("\t Pagada"));
	         }else{
	        	 document.add(new Paragraph("\t Pendiente de pago"));
	         }
	        
	         document.add(new Paragraph("\n"));
	         document.add(new Paragraph("-----------------------------------------------------"));
	         
	         document.add(new Paragraph("\n"));
	         document.add(new Paragraph("Nombre:", fontPrincipal));
	         document.add(new Paragraph("\t "+ data.getConfirmarData().getNombre() + " "+data.getConfirmarData().getApellidos()));
	         document.add(new Paragraph("Identificacion:", fontPrincipal));
	         document.add(new Paragraph("\t "+data.getConfirmarData().getIdentificacion()));
	         document.add(new Paragraph("Correo:", fontPrincipal));
	         document.add(new Paragraph("\t "+ data.getConfirmarData().getCorreo()));
	         
	        
	         
	         document.close();
	         writer.close();
	         
	         return pdf;
	      } catch (Exception e) {
	    	  throw new RuntimeException(e);
	      }
	}
}
