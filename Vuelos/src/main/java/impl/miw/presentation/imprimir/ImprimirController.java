package impl.miw.presentation.imprimir;

import java.io.File;
import java.io.FileNotFoundException;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("imprimir")
public class ImprimirController {
	
	@Autowired
	private CreatePDF horarios;
	
	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<FileSystemResource> get(Model model, HttpSession sesion) throws FileNotFoundException{
		
		ImprimirData datosImprimir = (ImprimirData) sesion.getAttribute("imprimirResultados");
		
		File pdf = horarios.createResultadosBusqueda(datosImprimir);
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.parseMediaType("application/pdf"));
		headers.add("Content-Disposition", "inline");
		return new ResponseEntity<FileSystemResource>(new FileSystemResource(pdf), headers, HttpStatus.OK);
	}
	
	@RequestMapping(value="reserva", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<FileSystemResource> imprimirReserva(Model model, HttpSession sesion) throws FileNotFoundException{
		
		ImprimirData datosImprimir = (ImprimirData) sesion.getAttribute("imprimirResultados");
		
		File pdf = horarios.createReservaConfirmada(datosImprimir);
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.parseMediaType("application/pdf"));
		headers.add("Content-Disposition", "inline");
		return new ResponseEntity<FileSystemResource>(new FileSystemResource(pdf), headers, HttpStatus.OK);
	}
}
