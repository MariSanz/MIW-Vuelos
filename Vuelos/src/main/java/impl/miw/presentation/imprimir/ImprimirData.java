package impl.miw.presentation.imprimir;

import impl.miw.presentation.buscador.ResultadoData;
import impl.miw.presentation.reservar.ConfirmacionData;

public class ImprimirData {
	
	private ResultadoData resultadoBusqueda;
	private ConfirmacionData confirmarData;
	
	public ResultadoData getResultadoBusqueda() {
		return resultadoBusqueda;
	}
	
	public void setResultadoBusqueda(ResultadoData resultadoBusqueda) {
		this.resultadoBusqueda = resultadoBusqueda;
	}
	
	public ConfirmacionData getConfirmarData() {
		return confirmarData;
	}
	
	public void setConfirmarData(ConfirmacionData confirmarData) {
		this.confirmarData = confirmarData;
	}
}
