package impl.miw.presentation.reservar;

import org.hibernate.validator.constraints.NotEmpty;

import com.miw.model.EstadoReserva;
import com.miw.model.TipoCoche;

public class ConfirmacionData {

	@NotEmpty
	private String nombre;
	@NotEmpty
	private String apellidos;
	private Integer tipoIdentificacion;
	@NotEmpty
	private String identificacion;
	@NotEmpty
	private String correo;
	private Boolean pagarReserva;
	private ReservaData reserva;
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	public Integer getTipoIdentificacion() {
		return tipoIdentificacion;
	}
	public void setTipoIdentificacion(Integer tipoIdentificacion) {
		this.tipoIdentificacion = tipoIdentificacion;
	}
	public String getIdentificacion() {
		return identificacion;
	}
	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}	
	public ReservaData getReserva() {
		return reserva;
	}	
	public void setReserva(ReservaData reserva) {
		this.reserva = reserva;
	}
	public Boolean getPagarReserva() {
		return pagarReserva;
	}
	public void setPagarReserva(Boolean pagarReserva) {
		this.pagarReserva = pagarReserva;
	}
	
	public TipoCoche getTipoCoche() {
		switch (getReserva().getCoche()) {
		case 0:
			return TipoCoche.NINGUNO;
		case 1:
			return TipoCoche.UTILITARIO;
		case 2:
			return TipoCoche.FURGONETA;
		default:
			throw new RuntimeException();
		}
	}
	
	public EstadoReserva getEstadoReserva() {
		if(!getPagarReserva()){
			return EstadoReserva.PENDIENTE_PAGO;
		}else{
			return EstadoReserva.PAGADA;
		}
	}
	
}