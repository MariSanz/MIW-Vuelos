package impl.miw.presentation.reservar;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class ValidadorConfirmacionReserva implements Validator {
	
	 private static final String PATTERN_EMAIL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
	            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

	@Override
	public boolean supports(Class<?> clazz) {
		return ConfirmacionData.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ConfirmacionData data = (ConfirmacionData)target;
		
		//validar nombre
		if(data.getNombre() != null){
			if ( data.getNombre().isEmpty() ) {
				errors.rejectValue("nombre", "confirmacion.nombre", "Nombre de la reserva no v�lido");
			}
		}else{
			errors.rejectValue("nombre", "confirmacion.nombre", "Nombre de la reserva no v�lido");
		}
		
		//Validar apellidos
		if(data.getApellidos() != null){
			if(data.getApellidos().isEmpty()){
				errors.rejectValue("apellidos", "confirmacion.apellidos", "Los apellidos de la reserva no v�lidos");
			}
		}else{
			errors.rejectValue("apellidos", "confirmacion.apellidos", "Los apellidos de la reserva no v�lidos");
		}
		
		//validamos el nif
		if(data.getIdentificacion() != null){
			if(data.getIdentificacion().isEmpty()){
				errors.rejectValue("identificacion", "confirmacion.identificacion", "El NIF es un dato obligatorio");
			}else{
				if(!isNifNie(data.getIdentificacion())){
					errors.rejectValue("identificacion", "confirmacion.nifNoValido", "No se corresponde a un NIF v�lido");
				}
			}
		}else{
			errors.rejectValue("identificacion", "confirmacion.identificacion", "El NIF es un dato obligatorio");
		}
		
		if(data.getCorreo() != null ){
			if(isValidEmail(data.getCorreo())){
				errors.rejectValue("correo", "confirmacion.correoNoValido", "El correo no tiene un formato v�lido");
			}			
		}else{
			errors.rejectValue("correo", "confirmacion.correo", "El correo es una informaci�n obligatoria");
		}
		
	}
	
	private boolean isNifNie(String nif) {

		// si es NIE, eliminar la x,y,z inicial para tratarlo como nif
		if (nif.toUpperCase().startsWith("X")
				|| nif.toUpperCase().startsWith("Y")
				|| nif.toUpperCase().startsWith("Z"))
			nif = nif.substring(1);

		Pattern nifPattern = Pattern
				.compile("(\\d{1,8})([TRWAGMYFPDXBNJZSQVHLCKEtrwagmyfpdxbnjzsqvhlcke])");
		Matcher m = nifPattern.matcher(nif);
		if (m.matches()) {
			String letra = m.group(2);
			// Extraer letra del NIF
			String letras = "TRWAGMYFPDXBNJZSQVHLCKE";
			int dni = Integer.parseInt(m.group(1));
			dni = dni % 23;
			String reference = letras.substring(dni, dni + 1);

			if (reference.equalsIgnoreCase(letra)) {
				return true;
			} else {
				return false;
			}
		} else
			return false;
	}
	
	 private boolean isValidEmail(String email) {
		 
	        Pattern pattern = Pattern.compile(PATTERN_EMAIL);
	 
	        Matcher matcher = pattern.matcher(email);
	        return matcher.matches();
	 
	    }

}
