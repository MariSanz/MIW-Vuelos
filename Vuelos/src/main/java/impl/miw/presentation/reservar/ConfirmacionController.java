package impl.miw.presentation.reservar;

import impl.miw.presentation.imprimir.ImprimirData;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.miw.business.ReservasService;
import com.miw.model.Reserva;

@Controller
@RequestMapping("reserva/confirmacion")
public class ConfirmacionController {
	
	@Autowired
	private ReservasService reservaService;
	
	@InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(new ValidadorConfirmacionReserva()); // registramos el validador
    }
	
	@RequestMapping(method = RequestMethod.POST)
	public String post(@Valid @ModelAttribute ConfirmacionData data, BindingResult result, Model model, HttpSession sesion){

		GuardarData gData = reservaService.guardar(data);
		data.getReserva().setCodigo(gData.getCodigoReserva());
		Reserva reservaFinal = reservaService.buscarPorCodigo(gData.getCodigoReserva());
		
		if(data.getPagarReserva()) {
			return "redirect:pago?cod="+gData.getCodigoReserva()+"&precio="+gData.getPrecioReserva();
			//return "iniciar_pago";
		}
		else {
			/*Mantengo en sesi�n los datos para imprimir resultados de busqueda*/
			ImprimirData imData = new ImprimirData();
			data.getReserva().setVueloIda(reservaFinal.getIda());
			data.getReserva().setVueloVuelta(reservaFinal.getVuelta());
			imData.setConfirmarData(data);
			sesion.setAttribute("imprimirResultados", imData);
			sesion.setAttribute("reserva", data);
			
			return "redirect:/reserva/confirmacion/exito";
		}
	}
	
	@RequestMapping(value = "exito", method = RequestMethod.GET)
	public String get(Model model, HttpSession sesion) {
		model.addAttribute("reserva", sesion.getAttribute("reserva"));
		return "reserva_exito";
	}
	

}
