package impl.miw.presentation.reservar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.miw.business.VuelosService;
import com.miw.model.Vuelo;

@Controller
@RequestMapping("reserva")
public class ReservaController {
	
	@Autowired
	private VuelosService vuelosService;

	@RequestMapping(method = RequestMethod.POST)
	public String post(@ModelAttribute ReservaData data, BindingResult result, Model model){
		
		Vuelo vueloIda = vuelosService.buscarPorId(data.getIdVueloIda());
		data.setVueloIda(vueloIda);
		Vuelo vueloVuelta = vuelosService.buscarPorId(data.getIdVueloVuelta());
		data.setVueloVuelta(vueloVuelta);
		
		model.addAttribute("reservarData", data);
		
		return "reserva";
	}
	
	@RequestMapping(value="datos", method = RequestMethod.POST)
	public String recogerDatos(@ModelAttribute ReservaData data, BindingResult result, Model model){
		
		Vuelo vueloIda = vuelosService.buscarPorId(data.getIdVueloIda());
		data.setVueloIda(vueloIda);
		Vuelo vueloVuelta = vuelosService.buscarPorId(data.getIdVueloVuelta());
		data.setVueloVuelta(vueloVuelta);

		model.addAttribute("reservarData", data);
		ConfirmacionData cData = new ConfirmacionData();
		cData.setReserva(data);
		model.addAttribute("confirmacionData", cData);
		
		return "confirmacion";
	}

}
