package impl.miw.presentation.reservar;

import org.hibernate.validator.constraints.NotEmpty;

public class CancelarData {
	
	@NotEmpty
	private String codigo;
	@NotEmpty
	private String emailAsociado;
	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getEmailAsociado() {
		return emailAsociado;
	}
	public void setEmailAsociado(String emailAsociado) {
		this.emailAsociado = emailAsociado;
	}
	
	

}
