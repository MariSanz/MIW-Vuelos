package impl.miw.presentation.reservar;

import javax.servlet.http.HttpSession;

import impl.miw.presentation.imprimir.ImprimirData;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.miw.business.ReservasService;
import com.miw.model.Reserva;

@Controller
@RequestMapping("reserva/pago")
public class PagarController {

	@Autowired
	private ReservasService reservaService;
	
	@RequestMapping(method = RequestMethod.GET)
	public String getRealizarPago(@RequestParam("cod") String codReserva, @RequestParam("precio") String precio, Model model)
	{
		model.addAttribute("codReserva", codReserva);
		model.addAttribute("precio", precio);
		return "iniciar_pago";
	}
	
	@RequestMapping(value="confirmado", method = RequestMethod.GET)
	public String getConfirmacionPago(@RequestParam("cod") String codReserva, Model model,  HttpSession sesion)
	{
		reservaService.actualizarEstadoPago(codReserva);
		
		Reserva r = reservaService.buscarPorCodigo(codReserva);
		
		/*Mantengo en sesi�n los datos para imprimir resultados de busqueda*/
		ImprimirData imData = new ImprimirData();
		ConfirmacionData data = new ConfirmacionData();
		data.setReserva(new ReservaData());
		data.getReserva().setVueloIda(r.getIda());
		data.getReserva().setVueloVuelta(r.getVuelta());
		data.getReserva().setCodigo(r.getCodigo());
		data.setNombre(r.getDatosDeCobro().getNombre());
		data.setApellidos(r.getDatosDeCobro().getApellidos());
		data.setCorreo(r.getEmailAsocido());
		data.getReserva().setNumMaletaNormal(r.getMaletasPequenas());
		data.getReserva().setNumMaletaGrande(r.getMaletasGrandes());
		imData.setConfirmarData(data);
		sesion.setAttribute("imprimirResultados", imData);
		model.addAttribute("reserva", data);
		return "reserva_exito";
		
	}
}
