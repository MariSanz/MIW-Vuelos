package impl.miw.presentation.reservar;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.miw.business.ReservasService;


@Controller
@RequestMapping("cancelarReserva")
public class CancelarReservaController {
	
	@Autowired
	private ReservasService reservaService;
	
	@RequestMapping(method = RequestMethod.GET)
	public String get(Model model)
	{
		model.addAttribute("cancelarForm",new CancelarData());		
		return "cancelar";
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public String post(@Valid @ModelAttribute CancelarData data, Model model)
	{
		
		reservaService.cancelarReserva(data.getCodigo(), data.getEmailAsociado());	
		model.addAttribute("message", "cancelar.exito");
		return "redirect:/";
	}

}
