package impl.miw.presentation.reservar;

import com.miw.model.Vuelo;


public class ReservaData {

	private Long idVueloIda;
	private Vuelo vueloIda;
	private Long idVueloVuelta;
	private Vuelo vueloVuelta;
	private String fechaIda;
	private String fechaVuelta;
	private Integer numMaletaNormal;
	private Integer numMaletaGrande;
	private Integer coche;
	private Integer plazas;
	private String codigo;
	
	public Long getIdVueloIda() {
		return idVueloIda;
	}
	
	public void setIdVueloIda(Long idVueloIda) {
		this.idVueloIda = idVueloIda;
	}
	
	public Long getIdVueloVuelta() {
		return idVueloVuelta;
	}
	
	public void setIdVueloVuelta(Long idVueloVuelta) {
		this.idVueloVuelta = idVueloVuelta;
	}
	
	public Integer getNumMaletaGrande() {
		return numMaletaGrande;
	}
	
	public void setNumMaletaGrande(Integer numMaletaGrande) {
		this.numMaletaGrande = numMaletaGrande;
	}
	
	public Integer getNumMaletaNormal() {
		return numMaletaNormal;
	}
	
	public void setNumMaletaNormal(Integer numMaletaNormal) {
		this.numMaletaNormal = numMaletaNormal;
	}
	
	public Vuelo getVueloIda() {
		return vueloIda;
	}
	
	public void setVueloIda(Vuelo vueloIda) {
		this.vueloIda = vueloIda;
	}
	
	public Vuelo getVueloVuelta() {
		return vueloVuelta;
	}
	
	public void setVueloVuelta(Vuelo vueloVuelta) {
		this.vueloVuelta = vueloVuelta;
	}
	
	public String getFechaIda() {
		return fechaIda;
	}
	
	public void setFechaIda(String fechaIda) {
		this.fechaIda = fechaIda;
	}
	
	public String getFechaVuelta() {
		return fechaVuelta;
	}
	
	public void setFechaVuelta(String fechaVuelta) {
		this.fechaVuelta = fechaVuelta;
	}
	
	public Integer getPlazas() {
		return plazas;
	}
	
	public void setPlazas(Integer plazas) {
		this.plazas = plazas;
	}
	
	public Integer getCoche() {
		return coche;
	}
	
	public void setCoche(Integer coche) {
		this.coche = coche;
	}
	
	public String getCodigo() {
		return codigo;
	}
	
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
}
