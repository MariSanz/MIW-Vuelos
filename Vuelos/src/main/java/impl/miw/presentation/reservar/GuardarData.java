package impl.miw.presentation.reservar;

public class GuardarData {

	private String codigoReserva;
	private Long idReserva;
	private Double precioReserva;
	public String getCodigoReserva() {
		return codigoReserva;
	}
	public void setCodigoReserva(String codigoReserva) {
		this.codigoReserva = codigoReserva;
	}
	public Long getIdReserva() {
		return idReserva;
	}
	public void setIdReserva(Long idReserva) {
		this.idReserva = idReserva;
	}
	public Double getPrecioReserva() {
		return precioReserva;
	}
	public void setPrecioReserva(Double precioReserva) {
		this.precioReserva = precioReserva;
	}
	
	
}
