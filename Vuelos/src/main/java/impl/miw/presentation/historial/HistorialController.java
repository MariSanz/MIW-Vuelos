package impl.miw.presentation.historial;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.miw.business.ReservasService;

@Controller
@RequestMapping("/private/historialVuelos")
public class HistorialController {
	
	@Autowired
	private ReservasService reservaService;

	@RequestMapping(method = RequestMethod.GET)
	public String get(Model model)
	{
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String usuario = auth.getName();
		
		List<HistorialData> datas = (List<HistorialData>) reservaService.reservasUsuario(usuario);
		
		model.addAttribute("historialData",datas);		
		return "private/historial";
	}
	
	@RequestMapping(value="cancelarReserva", method = RequestMethod.GET)
	public String cancelarReserva(@RequestParam("cod") String codigo, Model model){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String usuario = auth.getName();
		reservaService.cancelarReserva(codigo, usuario);
		model.addAttribute("mensaje", "cancelar.exito");
		return "private/historial";
	}
	
}
