package impl.miw.presentation.historial;

import com.miw.model.EstadoReserva;
import com.miw.model.TipoCoche;
import com.miw.model.Usuario;
import com.miw.model.Vuelo;

public class HistorialData {
	
	private String codigo;
	private String identificacion;
	private int maletasGrandes;
	private int maletasPequenas;
	private TipoCoche coche;
	private EstadoReserva estado;
	private Usuario usuario;
	private Vuelo ida;
	private Vuelo vuelta;
	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}
	public String getIdentificacion() {
		return identificacion;
	}
	public int getMaletasGrandes() {
		return maletasGrandes;
	}
	public void setMaletasGrandes(int maletasGrandes) {
		this.maletasGrandes = maletasGrandes;
	}
	public int getMaletasPequenas() {
		return maletasPequenas;
	}
	public void setMaletasPequenas(int maletasPequenas) {
		this.maletasPequenas = maletasPequenas;
	}
	public TipoCoche getCoche() {
		return coche;
	}
	public void setCoche(TipoCoche coche) {
		this.coche = coche;
	}
	public EstadoReserva getEstado() {
		return estado;
	}
	public void setEstado(EstadoReserva estado) {
		this.estado = estado;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public Vuelo getIda() {
		return ida;
	}
	public void setIda(Vuelo ida) {
		this.ida = ida;
	}
	public Vuelo getVuelta() {
		return vuelta;
	}
	public void setVuelta(Vuelo vuelta) {
		this.vuelta = vuelta;
	}
	
	

}
