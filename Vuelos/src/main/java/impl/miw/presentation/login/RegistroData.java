package impl.miw.presentation.login;

import org.hibernate.validator.constraints.NotEmpty;

import com.miw.model.Usuario;

public class RegistroData {
	
	@NotEmpty
	private String usuario;
	@NotEmpty
	private String email;
	@NotEmpty
	private String pass;
	@NotEmpty
	private String repPass;
	
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public String getRepPass() {
		return repPass;
	}
	public void setRepPass(String repPass) {
		this.repPass = repPass;
	}
	
	public Usuario toUsuario() {
		Usuario u = new Usuario(email);
		u.setNombre(usuario);
		u.setClave(pass);
		return u;
	}

}
