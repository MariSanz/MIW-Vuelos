package impl.miw.presentation.login;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.miw.business.UsuariosService;

@Controller
@RequestMapping("registro")
public class RegistroController {
	
	@Autowired
	private UsuariosService usuariosService;
	
	@RequestMapping(method = RequestMethod.POST)
	public String registro(@Valid @ModelAttribute RegistroData data, BindingResult br, Model model) {
		
		model.addAttribute("registroForm", new RegistroData());
		if (br.hasErrors()) {// hubo errores en los datos de la vista
			return "redirect:login";
		}
		
		
		if (!data.getPass().equals(data.getRepPass())) {
			model.addAttribute("error", "login.clavesNoCoinciden");
		} else if (data.getUsuario() == null) {
			model.addAttribute("error", "login.usuarioNecesario");
			br.addError(new FieldError("registroForm", "usuario", "{usuario_necesario}"));
		}
		try {
			// se trata el la logica de registro desde los servicios
			usuariosService.registrar(data);
			model.addAttribute("message", "login.registroExito");

		} catch (Exception e) {
			model.addAttribute("error", "login.usuarioRepetido");
		}

		return "login_page";
	}

}
