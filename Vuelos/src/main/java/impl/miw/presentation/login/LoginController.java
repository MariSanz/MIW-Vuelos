package impl.miw.presentation.login;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class LoginController {

	@RequestMapping(value="login", method = RequestMethod.GET)
	public String login(Model model) {
		
		model.addAttribute("registroForm", new RegistroData());
		return "login_page";
	}
	
	@RequestMapping(value="loginError", method = RequestMethod.GET)
	public String loginError(Model model) {
		
		model.addAttribute("registroForm", new RegistroData());
		return "login_error";
	}

}
