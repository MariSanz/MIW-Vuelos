package impl.miw.presentation.enviar;

import impl.miw.presentation.imprimir.ImprimirData;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.miw.business.EmailService;

@Controller
@RequestMapping("enviarReserva")
public class EnviarEmailController {
		
	@Autowired
	private EmailService emailService;
	
	@RequestMapping(method=RequestMethod.GET)
	public String enviarConfirmacionReserva(Model model, HttpSession session){
		ImprimirData datos = (ImprimirData) session.getAttribute("imprimirResultados");
		
		emailService.enviarConfirmacionReserva(datos.getConfirmarData());
		
		model.addAttribute("mensaje", "correo.exito");
		return "redirect:/";
	}
	
}
