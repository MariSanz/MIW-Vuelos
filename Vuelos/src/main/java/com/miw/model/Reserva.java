package com.miw.model;

import java.io.Serializable;
import java.util.Random;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(
		name = "RESERVA",
		uniqueConstraints = {
				@UniqueConstraint(columnNames = {"CODIGO"})
		})
public class Reserva implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;

	@Column(name = "CODIGO")
	private String codigo;
	
	@Column(name = "MALETAS_GRANDES")
	private int maletasGrandes;
	
	@Column(name = "MALETAS_NORMALES")
	private int maletasPequenas;

	@Enumerated(EnumType.STRING)
	@Column(name = "COCHE")
	private TipoCoche coche;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "ESTADO")
	private EstadoReserva estado;
	
	@Column(name = "EMAIL_ASOCIADO")
	private String emailAsocido;
	
	@Embedded
	private DatosDeCobro datosDeCobro = new DatosDeCobro();
	
	@ManyToOne
	@JoinColumn(name = "ID_IDA")
	private Vuelo ida;
	
	@ManyToOne
	@JoinColumn(name = "ID_VUELTA")
	private Vuelo vuelta;
	
	@ManyToOne
	@JoinColumn(name = "ID_USUARIO")
	private Usuario usuario;
	
	Reserva() {}
	
	private Reserva(String codigo) {
		this.codigo = codigo;
	}
	
	public static Reserva generar() {
		
		final String CARACTERES = "ABCEDFGHIJKLMNOPQRSTUVWXYZ0123456789";
		final Random RANDOM = new Random();
		
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < 5; i++) {
			sb.append(CARACTERES.charAt(RANDOM.nextInt(CARACTERES.length())));
		}
		
		return new Reserva(sb.toString());
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public int getMaletasGrandes() {
		return maletasGrandes;
	}

	public void setMaletasGrandes(int maletasGrandes) {
		this.maletasGrandes = maletasGrandes;
	}

	public int getMaletasPequenas() {
		return maletasPequenas;
	}

	public void setMaletasPequenas(int maletasPequenas) {
		this.maletasPequenas = maletasPequenas;
	}

	public TipoCoche getCoche() {
		return coche;
	}

	public void setCoche(TipoCoche coche) {
		this.coche = coche;
	}

	public EstadoReserva getEstado() {
		return estado;
	}

	public void setEstado(EstadoReserva estado) {
		this.estado = estado;
	}

	public Vuelo getIda() {
		return ida;
	}

	public void setIda(Vuelo ida) {
		this.ida = ida;
	}

	public Vuelo getVuelta() {
		return vuelta;
	}

	public void setVuelta(Vuelo vuelta) {
		this.vuelta = vuelta;
	}
	
	public Usuario getUsuario() {
		return usuario;
	}
	
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public DatosDeCobro getDatosDeCobro() {
		return datosDeCobro;
	}
	
	public void setDatosDeCobro(DatosDeCobro datosDeCobro) {
		this.datosDeCobro = datosDeCobro;
	}
	
	public String getEmailAsocido() {
		return emailAsocido;
	}
	
	public void setEmailAsocido(String emailAsocido) {
		this.emailAsocido = emailAsocido;
	}
	
	@Override
	public String toString() {
		return "Reserva [id=" + id + ", codigo=" + codigo + ", maletasGrandes="
				+ maletasGrandes + ", maletasPequenas=" + maletasPequenas
				+ ", coche=" + coche + ", estado=" + estado + ", datosDeCobro="
				+ datosDeCobro + ", ida=" + ida + ", vuelta=" + vuelta + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Reserva other = (Reserva) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		return true;
	}

}
