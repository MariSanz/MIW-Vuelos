package com.miw.model;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(
		name = "CIUDAD",
		uniqueConstraints = {
				@UniqueConstraint(columnNames = {"CODIGO"})
		})
public class Ciudad implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;
	
	@JsonProperty("label")
	@Column(name = "NOMBRE")
	private String nombre;
	
	@JsonIgnore
	@Column(name = "CODIGO")
	private String codigo;
	
	@Column(name = "FRECUENCIA")
	private Integer frecuencia;
	
	@JsonIgnore
	@OneToMany(mappedBy = "origen")
	private Set<Vuelo> vuelosOrigen = new HashSet<Vuelo>();
	
	@JsonIgnore
	@OneToMany(mappedBy = "destino")
	private Set<Vuelo> vuelosDestino = new HashSet<Vuelo>();

	Ciudad() {}
	
	public Ciudad(String nombre, String codigo) {
		this.nombre = nombre;
		this.codigo = codigo;
	}

	public Long getId() {
		return id;
	}

	public String getNombre() {
		return nombre;
	}

	public String getCodigo() {
		return codigo;
	}
	
	public Integer getFrecuencia() {
		return frecuencia;
	}
	
	public void setFrecuencia(Integer frecuencia) {
		this.frecuencia = frecuencia;
	}
	
	Set<Vuelo> _getVuelosOrigen() {
		return vuelosOrigen;
	}
	
	public Set<Vuelo> getVuelosOrigen() {
		return Collections.unmodifiableSet(vuelosOrigen);
	}
	
	Set<Vuelo> _getVuelosDestino() {
		return vuelosDestino;
	}
	
	public Set<Vuelo> getVuelosDestino() {
		return Collections.unmodifiableSet(vuelosDestino);
	}

	@Override
	public String toString() {
		return "Ciudad [id=" + id + ", nombre=" + nombre + ", codigo=" + codigo
				+ "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ciudad other = (Ciudad) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		return true;
	}
	
}
