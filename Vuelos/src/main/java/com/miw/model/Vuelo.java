package com.miw.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

@Entity
@Table(
		name = "VUELO",
		uniqueConstraints = {
				@UniqueConstraint(columnNames = {
						"FECHA",
						"ID_ORIGEN",
						"ID_DESTINO"
				})
		})
public class Vuelo implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA")
	private Date fecha;
	
	@ManyToOne
	@JoinColumn(name= "ID_ORIGEN")
	private Ciudad origen;
	
	@ManyToOne
	@JoinColumn(name = "ID_DESTINO")
	private Ciudad destino;
	
	@Column(name = "PLAZAS")
	private int plazas;
	
	Vuelo() {}

	public Vuelo(Date fecha, Ciudad origen, Ciudad destino, int plazas) {
		
		if (fecha == null || origen == null || destino == null) {
			throw new IllegalArgumentException("Falta un parametro obligatorio");
		}
		
		this.fecha = fecha;
		this.origen = origen;
		this.destino = destino;
		
		this.origen._getVuelosOrigen().add(this);
		this.destino._getVuelosOrigen().add(this);
	}

	public Long getId() {
		return id;
	}

	public Date getFecha() {
		return fecha;
	}

	public Ciudad getOrigen() {
		return origen;
	}

	public Ciudad getDestino() {
		return destino;
	}

	public int getPlazas() {
		return plazas;
	}
	
	public void setPlazas(int plazas) {
		this.plazas = plazas;
	}

	@Override
	public String toString() {
		return "Vuelo [id=" + id + ", fecha=" + fecha + ", origen=" + origen
				+ ", destino=" + destino + ", plazas=" + plazas + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((destino == null) ? 0 : destino.hashCode());
		result = prime * result + ((fecha == null) ? 0 : fecha.hashCode());
		result = prime * result + ((origen == null) ? 0 : origen.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vuelo other = (Vuelo) obj;
		if (destino == null) {
			if (other.destino != null)
				return false;
		} else if (!destino.equals(other.destino))
			return false;
		if (fecha == null) {
			if (other.fecha != null)
				return false;
		} else if (!fecha.equals(other.fecha))
			return false;
		if (origen == null) {
			if (other.origen != null)
				return false;
		} else if (!origen.equals(other.origen))
			return false;
		return true;
	}
	
}
