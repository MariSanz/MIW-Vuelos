package com.miw.model;

public enum EstadoReserva {

	PAGADA("Pagada"),
	PENDIENTE_PAGO("Pendiente pago"),
	CANCELADA("Cancelada");
	
	   private String nombre;

	   EstadoReserva(String name){nombre = name;}

	   @Override
	   public String toString() {
	       return nombre;
	   }
}
