package com.miw.model;

public class Mensaje {
	  private String miCorreo;
      private String miContraseņa;
      private String mailReceptor;
      private String asunto;
      private String cuerpo;
	public String getMiCorreo() {
		return miCorreo;
	}
	public void setMiCorreo(String miCorreo) {
		this.miCorreo = miCorreo;
	}
	public String getMiContraseņa() {
		return miContraseņa;
	}
	public void setMiContraseņa(String miContraseņa) {
		this.miContraseņa = miContraseņa;
	}
	public String getMailReceptor() {
		return mailReceptor;
	}
	public void setMailReceptor(String mailReceptor) {
		this.mailReceptor = mailReceptor;
	}
	public String getAsunto() {
		return asunto;
	}
	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}
	public String getCuerpo() {
		return cuerpo;
	}
	public void setCuerpo(String cuerpo) {
		this.cuerpo = cuerpo;
	}
      
      
}
