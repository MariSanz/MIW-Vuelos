package com.miw.model;

public enum TipoCoche {
	NINGUNO("Ninguno"),
	UTILITARIO("Utilitario"),
	FURGONETA("Furgoneta");
	
	 private String nombre;

	 TipoCoche(String name){nombre = name;}

	   @Override
	   public String toString() {
	       return nombre;
	   }
}
