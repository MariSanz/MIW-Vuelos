package com.miw.persistence;

import com.miw.model.Usuario;

public interface UsuariosDataService {

	Usuario buscarPorEmail(String email);

	void insertar(Usuario usuario);
}
