package com.miw.persistence;

import java.util.List;

import com.miw.model.Ciudad;

public interface CiudadesDataService {

	List<Ciudad> buscarPorNombre(String nombre);
	
	Ciudad buscarPorId(Long id);

	void actualizarFrecuencia(Long ciudadId);
	
	List<Ciudad> destinosFrecuentes();

	Long buscarVistas();

	Long buscarTotalCiudades();

}
