package com.miw.persistence;

import java.util.List;

import com.miw.model.Vuelo;

public interface VuelosDataService {

	List<Vuelo> buscarPorDestino(Long origen, String match);

	List<Vuelo> buscarPorTrayecto(Long origen, Long destino);

	Vuelo buscarPorId(Long idVueloIda);

	List<Vuelo> buscarPorTrayectoAndPlazas(Long origen, Long destino,
			Integer plazas);
}
