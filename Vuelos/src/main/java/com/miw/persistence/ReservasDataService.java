package com.miw.persistence;

import java.util.Collection;

import com.miw.model.Reserva;

public interface ReservasDataService {

	void insertar(Reserva reserva);

	Reserva buscarPorCodigo(String codigo);

	Collection<Reserva> buscarReservasPorUsuario(String usuario);

}
