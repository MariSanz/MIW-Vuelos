package com.miw.business;

import com.miw.model.Reserva;

import impl.miw.presentation.reservar.ConfirmacionData;

public interface EmailService {
	
	public void enviarConfirmacionReserva(ConfirmacionData datos);

	public void enviarCancelacionReserva(Reserva r);
}
