package com.miw.business;

import impl.miw.presentation.historial.HistorialData;
import impl.miw.presentation.reservar.ConfirmacionData;
import impl.miw.presentation.reservar.GuardarData;

import java.util.Collection;

import com.miw.model.Reserva;

public interface ReservasService {

	GuardarData guardar(ConfirmacionData data);

	void actualizarEstadoPago(String codReserva);

	Collection<HistorialData> reservasUsuario(String usuario);

	void cancelarReserva(String codigo, String email);

	Reserva buscarPorCodigo(String codigoReserva);

}
