package com.miw.business;

import java.util.List;

import com.miw.model.Vuelo;

public interface VuelosService {
	
	List<Vuelo> buscarPorDestino(Long nombre,  String match);

	List<Vuelo> buscarVuelos(Long origen, Long destino);
	
	Vuelo buscarPorId(Long idVueloIda);
	
}
