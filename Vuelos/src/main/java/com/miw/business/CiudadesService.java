package com.miw.business;

import impl.miw.presentation.buscador.FrecuenciaData;

import java.util.List;

import com.miw.model.Ciudad;

public interface CiudadesService {

	List<Ciudad> buscarPorNombre(String nombre);
	List<Ciudad> buscarCiudadVueloOrigen(String match);
	List<Ciudad> buscarCiudadVueloDestino(Long origen, String match);
	void actualizarFrecuencia(Long destino);
	FrecuenciaData buscarDestinosFrecuentes();

}
