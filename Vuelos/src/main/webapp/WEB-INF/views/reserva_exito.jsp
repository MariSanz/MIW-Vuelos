<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><spring:message code="titulo"/></title>

    <link href='<c:url value="/resources/bootstrap/css/bootstrap.min.css"/>' rel="stylesheet">
    <link rel="stylesheet" type="text/css" href='<c:url value="/resources/token-input/token-input.css"/>' />
    <link rel="stylesheet" type="text/css" href='<c:url value="/resources/css/funkyRadio.css"/>' />
    <link rel="stylesheet" type="text/css" href='<c:url value="/resources/bootstrap/css/bootstrap-datepicker.css"/>' />
    <link rel="stylesheet" type="text/css" href='<c:url value="/resources/bootstrap/css/bootstrap-datepicker.standalone.css"/>' />
   	<!-- Estilos personalizados -->
   	<link href='<c:url value="/resources/css/style.css"/>' rel="stylesheet">
    <!-- Fuentes de Google -->
    <link href="https://fonts.googleapis.com/css?family=Fjalla+One" rel="stylesheet">
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
    
  </head>
  <body>
  
  	    	<!--  Inicio NavBar -->
	  <nav class="navbar navbar-inverse">
	  <div class="container-fluid">
	    <!-- Banda para mostrarse en dispositivos moviles -->
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href='<c:url value="/"/>'><spring:message code="principal.nombre"/></a>
	    </div>
	
	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      <ul class="nav navbar-nav">
	        <li class="active"><a href='<c:url value="/"/>'><spring:message code="principal.buscar"/><span class="sr-only">(Actual)</span></a></li>
	        <security:authorize access="isAuthenticated()">
	     		<li><a href="<c:url value="/private/historialVuelos"/>"  role="button"><spring:message code="principal.historial"/></a></li>
	     	</security:authorize>
	     	<security:authorize access="!isAuthenticated()">
	     		<li><a href="cancelarReserva"  role="button"><spring:message code="principal.cancelar"/></a></li>
	     	</security:authorize>
	      </ul>
	     
	
	      <ul class="nav navbar-nav navbar-right">
	     	
	     	<security:authorize access="isAuthenticated()">
	     		<security:authentication var="principal" property="principal" />
	     		<li><a href="<c:url value="/"/>"  role="button"> ${principal.username} </a></li>
				<li>
					<a href='<c:url value="j_spring_security_logout"/>'  role="button"><spring:message code="login.logout"/></a>
				</li>
			</security:authorize>
			<security:authorize access="!isAuthenticated()">
				<li>
		      		<a href="<c:url value="login"/>"  role="button"><spring:message code="login_registro"/></a>
		      	</li>
		     </security:authorize>
		      	
		     
	       	 <li class="dropdown">
	          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
	          	<spring:message code="idioma"/>
	          	<span class="caret"></span>
	          </a>
	          <ul class="dropdown-menu">
	            <li><a href="?locale=es"><spring:message code="spanish"/></a></li>
	            <li><a href="?locale=en"><spring:message code="english"/></a></li>
	          </ul>
	        </li>
	        
	      </ul>
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>
  <!-- Fin NavBar -->
  <div class="container-fluid">
	    <h1 class="text-center typing-principal">
	    	<img class="img-reponsive logo" alt="Logo" src='<c:url value="/resources/image/logo_md.png"/>'>
	    	<spring:message code="principal.slogan"/>
	    </h1>
	    
	    <div class="row">
	    	<div class="col-md-6 col-xs-12 col-md-offset-3">
	    	<div class="panel panel-default">
			  <div class="panel-heading typing-principal avisos"><h2><spring:message code="reserva.confirmacion"/></h2></div>
			  <div class="panel-body">
			  		<div class="row">
			  			<div class="col-md-3">
			  				<label class="letra-media"><span class="label label-default"><spring:message code="confirmacion.nombre"/></span></label>
			  			</div>
			  			<div  class="col-md-4">
			  				<p class="letra-media">
				  				<c:out value="${reserva.nombre}"></c:out>
				  				<c:out value="${reserva.apellidos}"></c:out>
			  				</p>
			  			</div>
			  		</div>
			  		<div class="row">
			  			<div class="col-md-3">
			  				<label class="letra-media"><span class="label label-default"><spring:message code="confirmacion.identificacion"/></span></label>
			  			</div>
			  			<div  class="col-md-4">
			  				<p class="letra-media">
			  				<c:out value="${reserva.tipoIdentificacion}"></c:out>
			  				<c:out value="${reserva.identificacion}"></c:out>
			  				</p>
			  			</div>
			  		</div>
			  		<div class="row">
			  			<div class="col-md-3">
			  				<label class="letra-media"><span class="label label-default"><spring:message code="confirmacion.correo"/></span></label>
			  			</div>
			  			<div  class="col-md-5">
			  				<p class="letra-media">
			  				<c:out value="${reserva.correo}"></c:out>
			  				</p>
			  			</div>
			  		</div>
			  		<div class="row">
			  			<div class="col-md-3">
			  				<label class="letra-media"><span class="label label-default"><spring:message code="historial.vuelo.ida"/></span></label>
			  			</div>
			  			<div  class="col-md-5">
			  				<p class="letra-media">
			  				<c:out value="${reserva.reserva.vueloIda.origen.nombre}"></c:out>
			  				<c:out value="${reserva.reserva.fechaIda}"></c:out>
			  				</p>
			  			</div>
			  		</div>
			  		<c:if test="${not empty reserva.reserva.vueloVuelta}">
				  		<div class="row">
				  			<div class="col-md-3">
				  				<label class="letra-media"><span class="label label-default"><spring:message code="historial.vuelo.vuelta"/></span></label>
				  			</div>
				  			<div  class="col-md-5">
				  				<p class="letra-media">
					  				<c:out value="${reserva.reserva.vueloVuelta.origen.nombre}"></c:out>
					  				<c:out value="${reserva.reserva.fechaVuelta}"></c:out>
				  				</p>
				  			</div>
				  		</div>
			  		</c:if>
			  		<div class="row">
			  			<div class="col-md-3">
			  				<label class="letra-media"><span class="label label-default"><spring:message code="historial.codigo"/></span></label>
			  			</div>
			  			<div  class="col-md-5">
			  				<p class="letra-media">
			  				<c:out value="${reserva.reserva.codigo}"></c:out>
			  				</p>
			  			</div>
			  		</div>
			  		<div class="row">
			  			<div class="col-md-3">
			  				<label class="letra-media"><span class="label label-default"><spring:message code="confirmacion.maletasNormales"/></span></label>
			  			</div>
			  			<div  class="col-md-3">
			  				<p class="letra-media"><c:out value="${reserva.reserva.numMaletaNormal}"></c:out></p>
			  			</div>
			  		</div>
			  		<div class="row">
			  			<div class="col-md-3">
			  				<label class="letra-media"><span class="label label-default"><spring:message code="confirmacion.maletasGrandes"/></span></label>
			  			</div>
			  			<div  class="col-md-5">
			  				<p class="letra-media">
			  				<c:out value="${reserva.reserva.numMaletaGrande}"></c:out>
			  				</p>
			  			</div>
			  		</div>
			  		<div class="row">
			  			<div class="col-md-2">
			  				<a class="btn btn-default separar" href='<c:url value="/imprimir/reserva"/>' target="_blank">
			 				 	<i class="fa fa-print" aria-hidden="true"></i>
			 				 	<spring:message code="resultados.imprimir"/>
			 				 </a>
			  			</div>
			  			<div class="col-md-2">
			  				<a class="btn btn-default separar" href='<c:url value="/enviarReserva"/>'>
			 				 	<i class="fa fa-print" aria-hidden="true"></i>
			 				 	<spring:message code="confirmacion.enviarCorreo"/>
			 				 </a>
			  			</div>
			  		</div>
			  </div>
			</div>
	    	
	    	</div>
	    </div>
	   
	   <!-- Pie de p�gina -->
	<div id="footer">
		<div class="row botton">
			<div class="col-md-3 col-xs-12 text-center">
				<span class="glyphicon glyphicon-copyright-mark"></span>
				<strong><spring:message code="principal.copyright"/></strong>
			</div>
			<div class="col-md-4 hidden-xs text-center">
				<strong><spring:message code="principal.alumna"/></strong>
			</div>
			<div class="col-md-4 hidden-xs text-center">
				<strong><spring:message code="principal.master"/></strong>
			</div>
		</div>


	</div>
	<!-- Fin pie de p�gina --> 
	</div>

    <script src='<c:url value="/resources/jquery/jquery.js"/>' ></script>
    <script src='<c:url value="/resources/bootstrap/js/bootstrap.min.js"/>'></script>
    
    
  </body>
</html>