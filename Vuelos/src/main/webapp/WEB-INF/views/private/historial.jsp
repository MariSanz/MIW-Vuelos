<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><spring:message code="titulo"/></title>

    <!-- Fuentes de Google -->
    <link href="https://fonts.googleapis.com/css?family=Fjalla+One" rel="stylesheet">

    <link href='<c:url value="/resources/bootstrap/css/bootstrap.min.css"/>' rel="stylesheet">
    <link rel="stylesheet" type="text/css" href='<c:url value="/resources/token-input/token-input.css"/>' />
    <link rel="stylesheet" type="text/css" href='<c:url value="/resources/css/funkyRadio.css"/>' />
    <link rel="stylesheet" type="text/css" href='<c:url value="/resources/bootstrap/css/bootstrap-datepicker.css"/>' />
    <link rel="stylesheet" type="text/css" href='<c:url value="/resources/bootstrap/css/bootstrap-datepicker.standalone.css"/>' />
   	<!-- Estilos personalizados -->
   	<link href='<c:url value="/resources/css/style.css"/>' rel="stylesheet">
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
    
  </head>
  <body>
  
  	   	<!--  Inicio NavBar -->
	  <nav class="navbar navbar-inverse">
	  <div class="container-fluid">
	    <!-- Banda para mostrarse en dispositivos moviles -->
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href='<c:url value="/"/>'><spring:message code="principal.nombre"/></a>
	    </div>
	
	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      <ul class="nav navbar-nav">
	        <li class="active"><a href='<c:url value="/"/>'><spring:message code="principal.buscar"/><span class="sr-only">(Actual)</span></a></li>
	        <security:authorize access="isAuthenticated()">
	     		<li><a href="<c:url value="/private/historialVuelos"/>"  role="button"><spring:message code="principal.historial"/></a></li>
	     	</security:authorize>
	     	<security:authorize access="!isAuthenticated()">
	     		<li><a href="cancelarReserva"  role="button"><spring:message code="principal.cancelar"/></a></li>
	     	</security:authorize>
	      </ul>
	     
	
	      <ul class="nav navbar-nav navbar-right">
	     	
	     	<security:authorize access="isAuthenticated()">
	     		<security:authentication var="principal" property="principal" />
	     		<li><a href="<c:url value="/"/>"  role="button"> ${principal.username} </a></li>
				<li>
					<a href='<c:url value="j_spring_security_logout"/>'  role="button"><spring:message code="login.logout"/></a>
				</li>
			</security:authorize>
			<security:authorize access="!isAuthenticated()">
				<li>
		      		<a href="<c:url value="login"/>"  role="button"><spring:message code="login_registro"/></a>
		      	</li>
		     </security:authorize>
		      	
		     
	       	 <li class="dropdown">
	          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
	          	<spring:message code="idioma"/>
	          	<span class="caret"></span>
	          </a>
	          <ul class="dropdown-menu">
	            <li><a href="?locale=es"><spring:message code="spanish"/></a></li>
	            <li><a href="?locale=en"><spring:message code="english"/></a></li>
	          </ul>
	        </li>
	        
	      </ul>
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>
  <!-- Fin NavBar -->
  <div class="container-fluid">
	    <h1 class="text-center typing-principal">
	    	<img class="img-reponsive logo" alt="Logo" src='<c:url value="/resources/image/logo_md.png"/>'>
	    	<spring:message code="principal.slogan"/>
	    </h1>
	    
	    <div class="row">
	    	<div class="col-md-10 col-md-offset-1">
	    	<div class="panel panel-default">
			  <div class="panel-heading typing-principal avisos"><spring:message code="principal.historial.titulo"/></div>
			  <div class="panel-body">
			  	<div class="row">
					<div class="span5">
						<div class="table-responsive">
	            	  <table class="table table-striped table-condensed">
	                  <thead>
	                  <tr>
	                      <th class="text-center"><spring:message code="historial.codigo"/></th>
	                      <th class="text-center hidden-xs"><spring:message code="historial.dni"/></th>
	                      <th class="text-center"><spring:message code="historial.vuelo.ida"/></th>
	                      <th class="text-center"><spring:message code="historial.vuelo.vuelta"/></th> 
						  <th class="text-center hidden-xs"><spring:message code="historial.coche"/></th>  
						  <th class="text-center hidden-xs"><spring:message code="historial.reserva.estado"/></th>      
						  <th class="text-center"><spring:message code="historial.accion"/></th>                                   
	                  </tr>
	              </thead>   
	              <tbody>
              	    <c:forEach items="${historialData}" var="reserva">
             	    	<tr>
		                    <td class="text-center">
		                   		 <c:out value="${reserva.codigo}"></c:out>
		                    </td>
		                    <td class="text-center hidden-xs"><c:out value="${reserva.identificacion}"></c:out></td>
		                    <td>
		                    	<strong><spring:message code="buscador.origen"/></strong>
		                    	<c:out value="${reserva.ida.origen.nombre}"></c:out>
		                    	<br>
		                    	<strong><spring:message code="buscador.destino"/></strong>
		                    	<c:out value="${reserva.ida.destino.nombre}"></c:out>
		                    </td>
		                    <c:if test="${not empty reserva.vuelta}">
			                    <td>
			                    	<strong><spring:message code="buscador.origen"/></strong>
			                    	<c:out value="${reserva.ida.origen.nombre}"></c:out>
			                    	<br>
			                    	<strong><spring:message code="buscador.destino"/></strong>
			                    	<c:out value="${reserva.ida.destino.nombre}"></c:out>
			                    </td>
			                </c:if>
			                <c:if test="${empty reserva.vuelta}">
			                	 <td>---</td>
			                </c:if>
			                 <td class="text-center hidden-xs"><c:out value="${reserva.coche}"></c:out></td>
			                 <c:choose>
							    <c:when test="${reserva.estado eq 'CANCELADA'}">
							       <td class="text-center hidden-xs"><span class="label label-danger"><c:out value="${reserva.estado}"></c:out></span></td>   
							    </c:when> 
							    <c:when test="${reserva.estado eq 'PAGADA'}">
							       <td class="text-center hidden-xs"><span class="label label-success"><c:out value="${reserva.estado}"></c:out></span></td>   
							    </c:when> 
							    <c:when test="${reserva.estado eq 'PENDIENTE_PAGO'}">
							       <td class="text-center hidden-xs"><span class="label label-warning"><c:out value="${reserva.estado}"></c:out></span></td>   
							    </c:when>  
							    <c:otherwise>
							        <td class="text-center hidden-xs"><span class="label label-success"><c:out value="${reserva.estado}"></c:out></span></td>       
							    </c:otherwise>
							</c:choose>
		                    <td class="text-center">
		                    	<c:if test="${reserva.estado eq 'PENDIENTE_PAGO'}">
		                    		<button class="btn btn-default" data-codActual="${reserva.codigo}"  onclick="confirmarCancelar(this)"><spring:message code="historial.cancelar"/></button>
			                    	<!-- <spring:url value='historialVuelos/cancelarReserva?cod={codReserva}' var="url">
										<spring:param name="codReserva" value="${reserva.codigo}"/>
									</spring:url>
									<a href="${url}" class="btn btn-default" type="submit">
										<spring:message code="historial.cancelar"/>
									</a> -->
								</c:if>
								<c:if test="${not(reserva.estado eq 'PENDIENTE_PAGO')}">
									<spring:message code="historial.noDisponible"/>
								</c:if>
		                    </td>                                       
	               		 </tr>
              	    </c:forEach>
	              </tbody>
	            </table>
	            </div>
            </div>
			</div>
			  </div>
			</div>
	    	
	    	</div>
	    </div>
	    
	    <!-- Button modal -->
		<div id="cancelar_reserva" class="hide">
			<button id="cancelar_reserva_btn" type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#aceptar_cancelar">
			  Ver
			</button>
		</div>
	    <!-- Modal Confirmar Cancelacion Reserva -->
		<div class="modal fade" id="aceptar_cancelar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		        <h4 class="modal-title" id="myModalLabel">Cancelar reserva</h4>
		      </div>
		      <div class="modal-body">
		      	<div class="alert alert-success">
		      		<p>�Seguro desea cancelar su reserva <c:out value="${codReserva}"></c:out> ?</p>
		      	</div>
		      </div>
		      <div class="modal-footer">
		      	<button type="button" id="cerra_modal" class="btn btn-secondary" data-dismiss="modal"><spring:message code="historial.botonCancelar"/></button>
		        <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>  -->
		        <!--<spring:url value='historialVuelos/cancelarReserva?cod={codReserva}' var="url">
					<spring:param name="codReserva" value="${codReserva}"/>
				</spring:url>
				<a href="${url}" class="btn btn-default" type="submit">
					Aceptar
				</a> -->
		         <button type="button" onClick="cancelarReserva()"  class="btn btn-primary">Aceptar</button>
		      </div>
		    </div>
		  </div>
		</div>
	    
	    <!-- Pie de p�gina -->
	<div id="footer">
		<div class="row boton botton">
			<div class="col-md-3 col-xs-12 text-center">
				<span class="glyphicon glyphicon-copyright-mark"></span>
				<strong><spring:message code="principal.copyright"/></strong>
			</div>
			<div class="col-md-4 hidden-xs text-center">
				<strong><spring:message code="principal.alumna"/></strong>
			</div>
			<div class="col-md-4 hidden-xs text-center">
				<strong><spring:message code="principal.master"/></strong>
			</div>
		</div>


	</div>
	<!-- Fin pie de p�gina -->
	</div>

    <script src='<c:url value="/resources/jquery/jquery.js"/>' ></script>
    <script src='<c:url value="/resources/bootstrap/js/bootstrap.min.js"/>'></script>
    
    <script>
    	function confirmarCancelar(boton){
    		debugger;
    		var codigo = boton.getAttribute('data-codactual');
    		$('#cancelar_reserva_btn')[0].setAttribute('data-codigo', codigo);
    		$('#cancelar_reserva_btn').click();
    	}
    	
   		function cancelarReserva(){
   			var codigo = $('#cancelar_reserva_btn')[0].getAttribute('data-codigo');
   			var dir = '<c:url value="historialVuelos/cancelarReserva"/>?cod=' + codigo;
   			$.ajax({
    			type : "GET",
    			contentType : "application/json",
    			url : dir,
    			success : function(data) {
    				$('#cerrar_modal').click();
    			},
    			error : function(e) {
    				alert('<spring:message code="cancelar.exito"/>');
    			},
    			
    		});
   		
   		}
    
    </script>
    
  </body>
</html>