<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security"%>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><spring:message code="titulo"/></title>

    <link href='<c:url value="/resources/bootstrap/css/bootstrap.min.css"/>' rel="stylesheet">
    <link rel="stylesheet" type="text/css" href='<c:url value="/resources/token-input/token-input.css"/>' />
    <link rel="stylesheet" type="text/css" href='<c:url value="/resources/css/funkyRadio.css"/>' />
    <link rel="stylesheet" type="text/css" href='<c:url value="/resources/bootstrap/css/bootstrap-datepicker.css"/>' />
    <link rel="stylesheet" type="text/css" href='<c:url value="/resources/bootstrap/css/bootstrap-datepicker.standalone.css"/>' />
   	<!-- Estilos personalizados -->
   	<link href='<c:url value="/resources/css/style.css"/>' rel="stylesheet">
    <!-- Fuentes de Google -->
    <link href="https://fonts.googleapis.com/css?family=Fjalla+One" rel="stylesheet">
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
    
  </head>
  <body>
    
  	<!--  Inicio NavBar -->
	  <nav class="navbar navbar-inverse">
	  <div class="container-fluid">
	    <!-- Banda para mostrarse en dispositivos moviles -->
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href='<c:url value="/"/>'><spring:message code="principal.nombre"/></a>
	    </div>
	
	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      <ul class="nav navbar-nav">
	        <li class="active"><a href='<c:url value="/"/>'><spring:message code="principal.buscar"/><span class="sr-only">(Actual)</span></a></li>
	        <security:authorize access="isAuthenticated()">
	     		<li><a href="<c:url value="/private/historialVuelos"/>"  role="button"><spring:message code="principal.historial"/></a></li>
	     	</security:authorize>
	     	<security:authorize access="!isAuthenticated()">
	     		<li><a href="cancelarReserva"  role="button"><spring:message code="principal.cancelar"/></a></li>
	     	</security:authorize>
	      </ul>
	     
	
	      <ul class="nav navbar-nav navbar-right">
	     	
	     	<security:authorize access="isAuthenticated()">
	     		<security:authentication var="principal" property="principal" />
	     		<li><a href="<c:url value="/"/>"  role="button"> ${principal.username} </a></li>
				<li>
					<a href='<c:url value="j_spring_security_logout"/>'  role="button"><spring:message code="login.logout"/></a>
				</li>
			</security:authorize>
			<security:authorize access="!isAuthenticated()">
				<li>
		      		<a href="<c:url value="login"/>"  role="button"><spring:message code="login_registro"/></a>
		      	</li>
		     </security:authorize>
		      	
		     
	       	 <li class="dropdown">
	          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
	          	<spring:message code="idioma"/>
	          	<span class="caret"></span>
	          </a>
	          <ul class="dropdown-menu">
	            <li><a href="?locale=es"><spring:message code="spanish"/></a></li>
	            <li><a href="?locale=en"><spring:message code="english"/></a></li>
	          </ul>
	        </li>
	        
	      </ul>
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>
  <!-- Fin NavBar -->
  <div class="container-fluid">
  
	    <h1 class="text-center typing-principal">
	    	<img class="img-reponsive logo" alt="Logo" src='<c:url value="/resources/image/logo_md.png"/>'>
	    	<spring:message code="principal.slogan"/>
	    </h1>
	    
	    
	    <div class="row">
	    	<div class="col-md-10 col-md-offset-1">
			    <ul class="nav nav-tabs nav-justified">
				  <li class="disabled"><a href="#"><spring:message code="principal.panel.resultados"/> </a></li>	
				  <li class="disabled"><a href="#"><spring:message code="principal.panel.reserva"/></a></li>	
				  <li class="active"><a href="#"><spring:message code="principal.panel.confirmacion"/></a></li>
				  <li class="disabled"><a href="#"><spring:message code="principal.panel.pago"/></a></li>	  
				</ul>
			</div>
		</div>
			
			<div class="col-md-8 col-md-offset-2">
				<h2 class="text-center">Proceso confirmaci�n reserva</h2>
				<div class="row">
					<div class="col-md-2">
						<h3><span class="label label-primary">Vuelo de IDA</span></h3>
					</div>
					<div class="col-md-8 col-md-offset-2">
					    <h3>
					    	<c:out value="${reservarData.vueloIda.origen.nombre}"></c:out>	    	
					    	<c:out value="${reservarData.fechaIda}"></c:out>	
					    	a las <fmt:formatDate type="time" pattern="HH:mm"	    		
					    		value="${reservarData.vueloIda.fecha}"/>   	
				    	</h3>
		    		</div>
		    	</div>
		    	
		    	<c:if test="${not empty reservarData.vueloVuelta}">
		    		<div class="row">
		    			<div class="col-md-2">
			    		<h3><span class="label label-primary">Vuelo de VUELTA</span></h3>	
			    		</div>
			    		<div class="col-md-8 col-md-offset-2">
					    <h3>    	
					    	<c:out value="${reservarData.vueloIda.destino.nombre}"></c:out>	
					    	<c:out value="${reservarData.fechaVuelta}"></c:out>	
					    	a las <fmt:formatDate type="time" pattern="HH:mm"	    		
					    		value="${reservarData.vueloVuelta.fecha}"/>   	
			    		</h3>
			    		</div>
		    		</div>
		    	</c:if>
		    	
	    	<div class="panel panel-default">
				  <div class="panel-heading">
				    <h3 class="panel-title">Datos personales para la reserva</h3>
				  </div>
				  <div class="panel-body">
				 	<form:form commandName="confirmacionData" action="confirmacion">
				 		<div class="row">
				 			<div class="col-md-2">
					 			<div class="form-group">
								  <label for="ident">Identificacion</label>
								  <form:select path="tipoIdentificacion" cssClass="form-control" id="ident">
								    <option>NIF</option>
								    <option>Pasaporte</option>
								    <option>NIE</option>
								  </form:select>
								</div>
							</div>
							<div class="col-md-3">
					 			<div class="form-group">
								 	<label for="identificacion">N�mero</label>
							    	<form:input path="identificacion" cssClass="form-control" />
							    	<form:errors path="identificacion"/>								  
								</div>
							</div>
							<div class="col-md-5 col-xs-offset-1 col-md-offset-2">
								<div class="form-group">
									<label for="pagarReserva1" class="checkbox"> 
									<form:checkbox path="pagarReserva" />
										Pagar reserva ahora
									</label>	
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
						 		<div class="form-group">
								    <label for="nombre">Nombre</label>
								    <form:input path="nombre" required="required" cssClass="form-control" />	
								    <form:errors path="nombre"/>				   
							  	</div>
						  	</div>
						  	<div class="col-md-6">
							  	<div class="form-group">
								    <label for="apellidos">Apellidos</label>
								    <form:input path="apellidos" required="required" cssClass="form-control" />	
								    <form:errors path="apellidos"/>				   
							  	</div>
						  	</div>
					  	</div>
					  	<div class="row">
						  	<div class="col-md-6">
							  	<div class="form-group">
								    <label for="correo">E-mail</label>
								    <form:input type="email" required="required" path="correo" cssClass="form-control" />
								    <form:errors path="correo"/>					   
							  	</div>
						  	</div>
						  	<div class="col-md-2 col-xs-offset-7 col-md-offset-4 boton">
							  	<button type="submit" class="btn btn-primary">
									<i class="fa fa-chevron-right" aria-hidden="true"></i>
									Reservar
								</button>
							</div>
					  	</div>
					  	
					  	<form:hidden path="reserva.fechaIda" value="${reservaData.fechaIda}"></form:hidden>
						<form:hidden path="reserva.fechaVuelta" value="${reservaData.fechaVuelta}"></form:hidden>
						<form:hidden path="reserva.idVueloIda" value="${reservarData.vueloIda.id}"></form:hidden>
						<form:hidden path="reserva.idVueloVuelta" value="${reservarData.vueloVuelta.id}"></form:hidden>						
						<form:hidden path="reserva.coche" value="${reservarData.coche}"></form:hidden>
						<form:hidden path="reserva.numMaletaNormal" value="${reservarData.numMaletaNormal}"></form:hidden>
						<form:hidden path="reserva.numMaletaGrande" value="${reservarData.numMaletaGrande}"></form:hidden>
				 		
				 	</form:form>
				   	
				  </div>
			</div>
			  
			</div>
		
		<!-- Pie de p�gina -->
	<div id="footer">
		<div class="row boton botton">
			<div class="col-md-3 col-xs-12 text-center">
				<span class="glyphicon glyphicon-copyright-mark"></span>
				<strong><spring:message code="principal.copyright"/></strong>
			</div>
			<div class="col-md-4 hidden-xs text-center">
				<strong><spring:message code="principal.alumna"/></strong>
			</div>
			<div class="col-md-4 hidden-xs text-center">
				<strong><spring:message code="principal.master"/></strong>
			</div>
		</div>


	</div>
	<!-- Fin pie de p�gina -->
	</div>

    <script src='<c:url value="/resources/jquery/jquery.js"/>' ></script>
    <script src='<c:url value="/resources/bootstrap/js/bootstrap.min.js"/>'></script>
   
   
  </body>
</html>