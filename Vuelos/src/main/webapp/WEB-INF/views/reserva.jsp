<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security"%>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><spring:message code="titulo"/></title>

    <link href='<c:url value="/resources/bootstrap/css/bootstrap.min.css"/>' rel="stylesheet">
    <link rel="stylesheet" type="text/css" href='<c:url value="/resources/token-input/token-input.css"/>' />
    <link rel="stylesheet" type="text/css" href='<c:url value="/resources/css/funkyRadio.css"/>' />
    <link rel="stylesheet" type="text/css" href='<c:url value="/resources/bootstrap/css/bootstrap-datepicker.css"/>' />
    <link rel="stylesheet" type="text/css" href='<c:url value="/resources/bootstrap/css/bootstrap-datepicker.standalone.css"/>' />
   	<!-- Estilos personalizados -->
   	<link href='<c:url value="/resources/css/style.css"/>' rel="stylesheet">
    <!-- Fuentes de Google -->
    <link href="https://fonts.googleapis.com/css?family=Fjalla+One" rel="stylesheet">
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
    
  </head>
  <body>
    	<!--  Inicio NavBar -->
	  <nav class="navbar navbar-inverse">
	  <div class="container-fluid">
	    <!-- Banda para mostrarse en dispositivos moviles -->
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href='<c:url value="/"/>'><spring:message code="principal.nombre"/></a>
	    </div>
	
	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      <ul class="nav navbar-nav">
	        <li class="active"><a href='<c:url value="/"/>'><spring:message code="principal.buscar"/><span class="sr-only">(Actual)</span></a></li>
	        <security:authorize access="isAuthenticated()">
	     		<li><a href="<c:url value="/private/historialVuelos"/>"  role="button"><spring:message code="principal.historial"/></a></li>
	     	</security:authorize>
	     	<security:authorize access="!isAuthenticated()">
	     		<li><a href="cancelarReserva"  role="button"><spring:message code="principal.cancelar"/></a></li>
	     	</security:authorize>
	      </ul>
	     
	
	      <ul class="nav navbar-nav navbar-right">
	     	
	     	<security:authorize access="isAuthenticated()">
	     		<security:authentication var="principal" property="principal" />
	     		<li><a href="<c:url value="/"/>"  role="button"> ${principal.username} </a></li>
				<li>
					<a href='<c:url value="j_spring_security_logout"/>'  role="button"><spring:message code="login.logout"/></a>
				</li>
			</security:authorize>
			<security:authorize access="!isAuthenticated()">
				<li>
		      		<a href="<c:url value="login"/>"  role="button"><spring:message code="login_registro"/></a>
		      	</li>
		     </security:authorize>
		      	
		     
	       	 <li class="dropdown">
	          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
	          	<spring:message code="idioma"/>
	          	<span class="caret"></span>
	          </a>
	          <ul class="dropdown-menu">
	            <li><a href="?locale=es"><spring:message code="spanish"/></a></li>
	            <li><a href="?locale=en"><spring:message code="english"/></a></li>
	          </ul>
	        </li>
	        
	      </ul>
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>
  <!-- Fin NavBar -->
  <div class="container-fluid">
  
	    <h1 class="text-center typing-principal">
	    	<img class="img-reponsive logo" alt="Logo" src='<c:url value="/resources/image/logo_md.png"/>'>
	    	<spring:message code="principal.slogan"/>
	    </h1>
	    
	    
	    <div class="row">
	    	<div class="col-md-10 col-md-offset-1">
			    <ul class="nav nav-tabs nav-justified">
				  <li class="disabled"><a href="#"><spring:message code="principal.panel.resultados"/> </a></li>	
				  <li class="active"><a href="#"><spring:message code="principal.panel.reserva"/></a></li>	
				  <li class="disabled"><a href="#"><spring:message code="principal.panel.confirmacion"/></a></li>
				  <li class="disabled"><a href="#"><spring:message code="principal.panel.pago"/></a></li>
				</ul>
			</div>
		</div>
		<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<h2 class="text-center"><spring:message code="reserva.proceso"/> </h2>
			<div class="row">
				<div class="col-md-2">
					<h3>
				    	<span class="label label-primary"><spring:message code="reserva.vueloIDA"/> </span> 		
				    </h3>
				 </div>
				 <div class="col-md-8 col-md-offset-2">
				    <h3>
				    	<c:out value="${reservarData.vueloIda.origen.nombre}"></c:out>	    	
				    	<c:out value="${reservarData.fechaIda}"></c:out>	
				    	a las <fmt:formatDate type="time" pattern="HH:mm"	    		
				    		value="${reservarData.vueloIda.fecha}"/>   	
			    	</h3>
		    	</div>		    	
	    	</div>
		    	
	    	<c:if test="${not empty reservarData.vueloVuelta}">
	    		<div class="row">
				<div class="col-md-2">
		    		<h3>
				    	<span class="label label-primary"><spring:message code="reserva.vueloIDA"/></span> 	
				    </h3>	
			    </div>
			   	<div class="col-md-8 col-md-offset-2">
				    <h3>    	
				    	<c:out value="${reservarData.vueloIda.destino.nombre}"></c:out>	
				    	<c:out value="${reservarData.fechaVuelta}"></c:out>	
				    	a las <fmt:formatDate type="time" pattern="HH:mm"	    		
				    		value="${reservarData.vueloVuelta.fecha}"/>   	
		    		</h3>
	    		</div>
	    		</div>
	    	</c:if>
		    	
	    	<div class="panel panel-default">
				  <div class="panel-heading">
				    <h3 class="panel-title">Opcionales</h3>
				  </div>
				  <div class="panel-body">
				  	<form:form commandName="reservarData" action="reserva/datos">
					   	<h3>
					    	<span class="label label-default"><spring:message code="reserva.equipaje"/></span> 
					    </h3>
					    <div class="row">
						    <div class="col-md-3">
						    	<h4><spring:message code="reserva.maletaNormal"/> <span class="badge"><spring:message code="reserva.maletaNormalCap"/></span></h4>
					    	</div>
					    	<div class="col-md-3">
						    	<div class="input-group number-spinner">
									<span class="input-group-btn data-dwn">
										<button type="button" class="btn btn-default btn-info" data-dir="dwn"><span class="glyphicon glyphicon-minus"></span></button>
									</span>
									<form:input type="text" path="numMaletaNormal" class="form-control text-center" value="0" min="0" max="4"></form:input>
									<span class="input-group-btn data-up">
										<button type="button" class="btn btn-default btn-info" data-dir="up"><span class="glyphicon glyphicon-plus"></span></button>
									</span>
								</div>
						    </div>
					    </div>
					    <div class="row boton">
						    <div class="col-md-3">						  
						    	<h4><spring:message code="reserva.maletaGrande"/><span class="badge"> <spring:message code="reserva.maletaGrandeCap"/></span></h4>
					    	</div>
					    	<div class="col-md-3">
						    	<div class="input-group number-spinner">
									<span class="input-group-btn data-dwn">
										<button type="button" class="btn btn-default btn-info" data-dir="dwn"><span class="glyphicon glyphicon-minus"></span></button>
									</span>
									<form:input type="text" path="numMaletaGrande" class="form-control text-center" value="0" min="0" max="4"></form:input>
									<span class="input-group-btn data-up">
										<button type="button" class="btn btn-default btn-info" data-dir="up"><span class="glyphicon glyphicon-plus"></span></button>
									</span>
								</div>
						    </div>
					    </div>
					    
					    <h3>
					    	<span class="label label-default"><spring:message code="reserva.alquilerCoche"/></span> 
					    </h3>
					    <div class="row">
					   
						    <div class="funkyradio">
						    	<div class="col-md-4">
							    <div class="funkyradio-info">
							    	<form:radiobutton path="coche"  value="0" id="ninguno" checked="checked"/>
						            <label for="ninguno"><spring:message code="reserva.cocheNinguno"/></label>
						         </div>						         
						         </div>
						         
						         <div class="col-md-4">
						         <div class="funkyradio-info">   
						            <form:radiobutton path="coche"  value="1" id="utilitario"/>
						            <label for="utilitario"><spring:message code="reserva.cocheUtilitario"/></label>
						         </div>
						         </div>
						         
						         <div class="col-md-4">
						         <div class="funkyradio-info">
						            <form:radiobutton path="coche"  value="2" id="furgoneta"/>
						            <label for="furgoneta"><spring:message code="reserva.cocheFurgoneta"/></label>					            
								 </div>
								 </div>
							</div>
						</div>
					    <form:hidden path="fechaIda" value="${reservaData.fechaIda}"></form:hidden>
						<form:hidden path="fechaVuelta" value="${reservaData.fechaVuelta}"></form:hidden>
						<form:hidden path="idVueloIda" value="${reservarData.vueloIda.id}"></form:hidden>
						<form:hidden path="idVueloVuelta" value="${reservarData.vueloVuelta.id}"></form:hidden>						
						
						<div class="row">
				 			
					 		<div class="col-md-2 col-xs-offset-8 col-md-offset-10">					
								<button type="submit" class="btn btn-primary separar">
									<i class="fa fa-chevron-right" aria-hidden="true"></i>
									<spring:message code="resultados.siguiente"/>
								</button>								
							</div>
					  </div>
					
				    </form:form>
				   
				  </div>
			</div>
			  
		</div>
		</div>
		
		<!-- Pie de p�gina -->
	<div id="footer">
		<div class="row botton">
			<div class="col-md-3 col-xs-12 text-center">
				<span class="glyphicon glyphicon-copyright-mark"></span>
				<strong><spring:message code="principal.copyright"/></strong>
			</div>
			<div class="col-md-4 hidden-xs text-center">
				<strong><spring:message code="principal.alumna"/></strong>
			</div>
			<div class="col-md-4 hidden-xs text-center">
				<strong><spring:message code="principal.master"/></strong>
			</div>
		</div>


	</div>
	<!-- Fin pie de p�gina -->
	</div>

    <script src='<c:url value="/resources/jquery/jquery.js"/>' ></script>
    <script src='<c:url value="/resources/bootstrap/js/bootstrap.min.js"/>'></script>
    
    <script>
    $(document).on('click', '.number-spinner button', function () {    
    	var btn = $(this),
    		input = btn.closest('.number-spinner').find('input'),
    		oldValue = input.val().trim(),
    		newVal = 0,
    		maxValue = parseInt(input.attr('max'));
    	
    	if (btn.attr('data-dir') == 'up') {
    			if(oldValue < maxValue){
    				newVal = parseInt(oldValue) + 1;
    			}   			
    		
    	} else {
    		if (oldValue > 1) {
    			newVal = parseInt(oldValue) - 1;
    			
    		} else {
    			newVal = 0;
    			//btn.prop("disabled", true);
    		}
    	}
    	btn.closest('.number-spinner').find('input').val(newVal);
    });
    </script>
   
  </body>
</html>