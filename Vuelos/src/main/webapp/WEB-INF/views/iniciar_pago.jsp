<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security"%>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><spring:message code="titulo"/></title>

    <link href='<c:url value="/resources/bootstrap/css/bootstrap.min.css"/>' rel="stylesheet">
    <link rel="stylesheet" type="text/css" href='<c:url value="/resources/token-input/token-input.css"/>' />
    <link rel="stylesheet" type="text/css" href='<c:url value="/resources/bootstrap/css/bootstrap-datepicker.css"/>' />
    <link rel="stylesheet" type="text/css" href='<c:url value="/resources/bootstrap/css/bootstrap-datepicker.standalone.css"/>' />
   	<!-- Estilos personalizados -->
   	<link href='<c:url value="/resources/css/style.css"/>' rel="stylesheet">
    <!-- Fuentes de Google -->
    <link href="https://fonts.googleapis.com/css?family=Fjalla+One" rel="stylesheet">
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
    
    <script src="https://www.paypalobjects.com/api/checkout.js"></script>
    
  </head>
  <body>
  	
  	
  	   	<!--  Inicio NavBar -->
	  <nav class="navbar navbar-inverse">
	  <div class="container-fluid">
	    <!-- Banda para mostrarse en dispositivos moviles -->
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href='<c:url value="/"/>'><spring:message code="principal.nombre"/></a>
	    </div>
	
	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      <ul class="nav navbar-nav">
	        <li class="active"><a href='<c:url value="/"/>'><spring:message code="principal.buscar"/><span class="sr-only">(Actual)</span></a></li>
	        <security:authorize access="isAuthenticated()">
	     		<li><a href="<c:url value="/private/historialVuelos"/>"  role="button"><spring:message code="principal.historial"/></a></li>
	     	</security:authorize>
	     	<security:authorize access="!isAuthenticated()">
	     		<li><a href="cancelarReserva"  role="button"><spring:message code="principal.cancelar"/></a></li>
	     	</security:authorize>
	      </ul>
	     
	
	      <ul class="nav navbar-nav navbar-right">
	     	
	     	<security:authorize access="isAuthenticated()">
	     		<security:authentication var="principal" property="principal" />
	     		<li><a href="<c:url value="/"/>"  role="button"> ${principal.username} </a></li>
				<li>
					<a href='<c:url value="j_spring_security_logout"/>'  role="button"><spring:message code="login.logout"/></a>
				</li>
			</security:authorize>
			<security:authorize access="!isAuthenticated()">
				<li>
		      		<a href="<c:url value="login"/>"  role="button"><spring:message code="login_registro"/></a>
		      	</li>
		     </security:authorize>
		      	
		     
	       	 <li class="dropdown">
	          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
	          	<spring:message code="idioma"/>
	          	<span class="caret"></span>
	          </a>
	          <ul class="dropdown-menu">
	            <li><a href="?locale=es"><spring:message code="spanish"/></a></li>
	            <li><a href="?locale=en"><spring:message code="english"/></a></li>
	          </ul>
	        </li>
	        
	      </ul>
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>
  <!-- Fin NavBar -->
  <div class="container-fluid">
  
	    <h1 class="text-center typing-principal">
	    	<img class="img-reponsive logo" alt="Logo" src='<c:url value="/resources/image/logo_md.png"/>'>
	    	<spring:message code="principal.slogan"/>
	    </h1>
	    
	    
	    <div class="row">
	    	<div class="col-md-10 col-md-offset-1">
			    <ul class="nav nav-tabs nav-justified">
				 <li class="disabled"><a href="#"><spring:message code="principal.panel.resultados"/> </a></li>	
				  <li class="disabled"><a href="#"><spring:message code="principal.panel.reserva"/></a></li>	
				  <li class="disabled"><a href="#"><spring:message code="principal.panel.confirmacion"/></a></li>
				  <li class="active"><a href="#"><spring:message code="principal.panel.pago"/></a></li>	  
				</ul>
			</div>
		</div>
		<div class="row">	
			<div class="col-md-8 col-md-offset-2">
				<h2 class="text-center">Proceso confirmación reserva</h2>
			</div>
		</div>
		<div class="row">	
			<div class="col-md-3 col-xs-8 col-md-offset-2">
				<h3>Código de reserva</h3>
			</div>	   
			<div class="col-md-1 col-xs-4"> 	
		    	<h3><c:out value="${codReserva}"></c:out></h3>
		   	</div>
		</div>
		<div class="row">
		    <div class="col-md-4 col-xs-3 col-xs-offset-1 col-md-offset-2"> 
		    	<h3><c:out value="${precio}"></c:out> &#8364;</h3>	 	
			 </div>
			 <div class="col-md-4 col-xs-4"> 
		    	<div id="paypal-button" class="boton"></div>	 	
			 </div>
		</div>
		
		<!-- Button modal -->
		<div id="pago_aceptado" class="hide">
			<button id="pago_aceptado_btn" type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#aceptar_pago">
			  Aceptar
			</button>
		</div>
		
		<!-- Modal -->
		<div class="modal fade" id="aceptar_pago" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		        <h4 class="modal-title" id="myModalLabel">Informacion de pago</h4>
		      </div>
		      <div class="modal-body">
		      	<div class="alert alert-success">
		      		<p>Pago de la reserva código <c:out value="${codReserva}"></c:out> realizado correctamente</p>
		      	</div>
		      </div>
		      <div class="modal-footer">
		        <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>  -->
		        <spring:url value='pago/confirmado?cod={codReserva}' var="url">
					<spring:param name="codReserva" value="${codReserva}"/>
				</spring:url>
				<a href="${url}" class="btn btn-default" type="submit">
					Aceptar
				</a>
		        <!-- <button type="button" class="btn btn-primary">Aceptar</button> -->
		      </div>
		    </div>
		  </div>
		</div>
		
		<!-- Pie de página -->
	<div id="footer">
		<div class="row boton botton">
			<div class="col-md-3 col-xs-12 text-center">
				<span class="glyphicon glyphicon-copyright-mark"></span>
				<strong><spring:message code="principal.copyright"/></strong>
			</div>
			<div class="col-md-4 hidden-xs text-center">
				<strong><spring:message code="principal.alumna"/></strong>
			</div>
			<div class="col-md-4 hidden-xs text-center">
				<strong><spring:message code="principal.master"/></strong>
			</div>
		</div>


	</div>
	<!-- Fin pie de página -->
	</div>

    <script src='<c:url value="/resources/jquery/jquery.js"/>' ></script>
    <script src='<c:url value="/resources/bootstrap/js/bootstrap.min.js"/>'></script>
  	
  
  	<script>
    paypal.Button.render({
        
        env: 'sandbox', // Specify 'sandbox' for the test environment
        
        style: {
            size: 'medium',
            color: 'orange',
            shape: 'pill'
        },
        
        client: {
            sandbox:    'AbiF-bY0LEx52qPRBFwPIIdyWs3o1rQjGZPpBp2t_wX6YsjH76NFTumFO66sjLINN3orWEg8nTLecCjA',            
        },

        payment: function() {
            var env    = this.props.env;
            var client = this.props.client;
        
            return paypal.rest.payment.create(env, client, {
                transactions: [
                    {
                        amount: { total: '<c:out value="${precio}"></c:out>', currency: 'EUR' }
                    }
                ]
            });
        },
        
        commit: true,

        onAuthorize: function(data, actions) {
        	 return actions.payment.execute().then(function() {
                 //location.url = '<c:url value="/"/>';
        		$('#pago_aceptado_btn').click();
             });
       }
            
    }, '#paypal-button');
  	</script>
  </body>
  