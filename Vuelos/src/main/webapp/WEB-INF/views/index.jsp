<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security"%>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><spring:message code="titulo"/></title>

    <link href='<c:url value="/resources/bootstrap/css/bootstrap.min.css"/>' rel="stylesheet">
    <link rel="stylesheet" type="text/css" href='<c:url value="/resources/token-input/token-input.css"/>' />
    <link rel="stylesheet" type="text/css" href='<c:url value="/resources/css/funkyRadio.css"/>' />
    <link rel="stylesheet" type="text/css" href='<c:url value="/resources/bootstrap/css/bootstrap-datepicker.css"/>' />
    <link rel="stylesheet" type="text/css" href='<c:url value="/resources/bootstrap/css/bootstrap-datepicker.standalone.css"/>' />
   	<!-- Estilos personalizados -->
   	<link href='<c:url value="/resources/css/style.css"/>' rel="stylesheet">
    <!-- Fuentes de Google -->
    <link href="https://fonts.googleapis.com/css?family=Fjalla+One" rel="stylesheet">
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
    
  </head>
  <body>
  
  	<!--  Inicio NavBar -->
	  <nav class="navbar navbar-inverse">
	  <div class="container-fluid">
	    <!-- Banda para mostrarse en dispositivos moviles -->
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href='<c:url value="/"/>'><spring:message code="principal.nombre"/></a>
	    </div>
	
	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      <ul class="nav navbar-nav">
	        <li class="active"><a href='<c:url value="/"/>'><spring:message code="principal.buscar"/><span class="sr-only">(Actual)</span></a></li>
	        <security:authorize access="isAuthenticated()">
	     		<li><a href="<c:url value="/private/historialVuelos"/>"  role="button"><spring:message code="principal.historial"/></a></li>
	     	</security:authorize>
	     	<security:authorize access="!isAuthenticated()">
	     		<li><a href="cancelarReserva"  role="button"><spring:message code="principal.cancelar"/></a></li>
	     	</security:authorize>
	      </ul>
	     
	
	      <ul class="nav navbar-nav navbar-right">
	     	
	     	<security:authorize access="isAuthenticated()">
	     		<security:authentication var="principal" property="principal" />
	     		<li><a href="<c:url value="/"/>"  role="button"> ${principal.username} </a></li>
				<li>
					<a href='<c:url value="j_spring_security_logout"/>'  role="button"><spring:message code="login.logout"/></a>
				</li>
			</security:authorize>
			<security:authorize access="!isAuthenticated()">
				<li>
		      		<a href="<c:url value="login"/>"  role="button"><spring:message code="login_registro"/></a>
		      	</li>
		     </security:authorize>
		      	
		     
	       	 <li class="dropdown">
	          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
	          	<spring:message code="idioma"/>
	          	<span class="caret"></span>
	          </a>
	          <ul class="dropdown-menu">
	            <li><a href="?locale=es"><spring:message code="spanish"/></a></li>
	            <li><a href="?locale=en"><spring:message code="english"/></a></li>
	          </ul>
	        </li>
	        
	      </ul>
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>
  <!-- Fin NavBar -->
  <div class="container-fluid">
	    <h1 class="text-center typing-principal">
	    	<img class="img-reponsive logo" alt="Logo" src='<c:url value="/resources/image/logo_md.png"/>'>
	    	<spring:message code="principal.slogan"/>
	    </h1>
	    
		<c:if test="${not empty error}">
			<div class="alert alert-danger">
			  <strong><spring:message code="principal.error"/></strong> <spring:message code="${error}"/>
			</div>
		</c:if>	    
		
		<c:if test="${not empty message}">
			<div class="alert alert-info">
			  <strong><spring:message code="principal.informacion"/></strong> <spring:message code="${message}"/>
			</div>
		</c:if>	 
		
	    <div class="row">
	    	<div class="col-md-6">
	    	<div class="panel panel-default">
			  <div class="panel-heading typing-principal avisos"><h2><spring:message code="principal.slogan.buscar"/></h2></div>
			  <div class="panel-body">
			  
			<form:form commandName="buscadorData">
					
					
				<div class="funkyradio">
					  <div class="row">
							<div class="col-md-6">
								<div class="form-group">	
						        <div class="funkyradio-info">
						            <form:radiobutton path="idaVuelta" value="true" name="idaVuelta" id="radio1" checked="checked"/>
						            <label for="radio1"><spring:message code="buscador.idaVuelta"/></label>
						        </div>
						       	</div>
						     </div>
						     <div class="col-md-6">
						     	<div class="form-group">	
							        <div class="funkyradio-info">
							            <form:radiobutton path="idaVuelta" value="false" name="idaVuelta" id="radio2" />
							            <label for="radio2"><spring:message code="buscardor.ida"/></label>
							        </div>
						        </div>
					    	</div>
		        		</div>
		        	</div>
		        	
		        	
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="text-origen"><spring:message code="buscador.origen"/></label>
								<form:input path="origen" type="text"  id="text-origen" cssClass="form-control"/>							
								<form:errors path="origen"/>
							</div>
						</div>
						
						<div class="col-md-6">					
							<div class="form-group">
								<label for="text-destino"><spring:message code="buscador.destino"/></label>
								<form:input path="destino" type="text" id="text-destino" cssClass="form-control"/>						
								<form:errors path="destino"/>
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-6">	
							<div class="form-group">	
								<label for="fechaIda"><spring:message code="buscador.fecha.ida"/></label>
								<div class="input-group date" id="fechaIda" data-provide="datepicker">
								    <input type="text" class="form-control" id="fecha-ida">
								    <form:hidden path="fechaIda" id="hidden-fecha-ida" />
								    <div class="input-group-addon">
								        <span class="glyphicon glyphicon-th"></span>
								    </div>
								</div>
							</div>
						</div>
						
						<div class="col-md-6">
							 <div class="form-group">	
								<label for="fechaVuelta"><spring:message code="buscador.fecha.vuelta"/></label>
								<div class="input-group date" id="fechaVuelta" data-provide="datepicker">
								    <input type="text" class="form-control" id="fecha-vuelta">
								    <form:hidden path="fechaVuelta" id="hidden-fecha-vuelta" />
								    <div class="input-group-addon">
								        <span class="glyphicon glyphicon-th"></span>
								    </div>
								</div>
							</div>
						</div>
					</div>
					
					<div class="row">
	   					<div class="col-md-3 col-md-offset-6">				
							<button type="submit" class="btn btn-primary form-control"><p class="letra-media typing-principal"><spring:message code="buscador.buscar"/></p></button>
						</div>
					</div>
				
				</form:form>
				</div>
			</div>
	    	
	    	</div>
	    	<div class="col-md-6">
	    		<!-- Carrousel -->
				 <div id="outerWrapper">
				    <div class="slide-1">
				    	<img src='<c:url value="/resources/image/cities/asturias.jpg"/>'/> 
				    </div>
				    <div class="slide-1"><img src='<c:url value="/resources/image/cities/madrid.jpg"/>'/>  </div>
				    <div class="slide-1"><img src='<c:url value="/resources/image/cities/barcelona.jpg"/>'/>  </div>
				    <div class="slide-1"><img src='<c:url value="/resources/image/cities/paris.jpg"/>'/> </div>
				    <div class="slide-1"><img src='<c:url value="/resources/image/cities/croacia.jpg"/>'/> </div>
			    </div>
			    <!-- Fin Carrousel -->
			
	    	</div>
	    </div>
	    
	     <div class="row boton">
	    	<div class="col-md-12">
		    <!-- Tabla 5 destinos m�s frecuentes -->
		    <div class="panel panel-primary" >
	    		<div class="panel-heading">
				  	<h3 class="panel-title">
				  		<spring:message code="principal.masBuscados"/>
				    </h3>    
	  			</div>     
				<div class="panel-body" >
		  
				  <div id="Main">
				    <c:forEach items="${destinosFrecuentes.destinos}" var="d">
				    	<a data-name="poll_bar">${d.destino.nombre} </a> <span data-name="poll_val">${d.frecuencia}% </span><br/>
				    </c:forEach>
			    </div>
				</div>
			</div>
		</div>
		</div>

<!-- Pie de p�gina -->
	<div id="footer">
		<div class="row boton botton">
			<div class="col-md-3 col-xs-12 text-center">
				<span class="glyphicon glyphicon-copyright-mark"></span>
				<strong><spring:message code="principal.copyright"/></strong>
			</div>
			<div class="col-md-4 hidden-xs text-center">
				<strong><spring:message code="principal.alumna"/></strong>
			</div>
			<div class="col-md-4 hidden-xs text-center">
				<strong><spring:message code="principal.master"/></strong>
			</div>
		</div>


	</div>
	<!-- Fin pie de p�gina -->		
	</div>

    <script src='<c:url value="/resources/jquery/jquery.js"/>' ></script>
    <script src='<c:url value="/resources/bootstrap/js/bootstrap.min.js"/>'></script>
    
    <!-- Libreria de desplegable seleccion -->
    <script src='<c:url value="/resources/token-input/jquery.tokeninput.js"/>' ></script>
    
    <!-- Libreria del input calendar -->
    <script src='<c:url value="/resources/bootstrap/js/bootstrap-datepicker.js"/>'></script>
    <script src='<c:url value="/resources/bootstrap/js/bootstrap-datepicker.es.min.js"/>' charset = "UTF-8"></script>
    
    <!-- Libreria del slider -->
    <script src='<c:url value="/resources/js/loopit.js"/>'></script>
    
    <!-- Libreria Spinner -->
     <script src='<c:url value="/resources/js/spinner.js"/>'></script>
    <script>
		$( function() {
			
			var urlDestino = '';
			
			function getUrlDestino() {
				return urlDestino;
			}
			
            $('#text-destino').tokenInput(getUrlDestino, {
				tokenLimit: 1,
				propertyToSearch: 'label',
				hintText: '<spring:message code="buscador.introduceOrigen"/>', 
				noResultsText : '<spring:message code="buscador.noResultados"/>',
				searchingText: '<spring:message code="buscador.buscando"/> ...',
			});
            $('#token-input-text-destino').prop('disabled', true)

			$('#text-origen').tokenInput('<c:url value="/ciudadesOrigen"/>', {
				tokenLimit: 1,
				propertyToSearch: 'label',
				hintText: '<spring:message code="buscador.introduceOrigen"/>',
				searchingText: '<spring:message code="buscador.buscando"/> ...',
				noResultsText : '<spring:message code="buscador.noResultados"/>',
				onAdd: function (item) {
					urlDestino = '<c:url value="/ciudadesDestino"/>?o=' + item.id;
		            $('#token-input-text-destino').prop('disabled', false)
                },
                onDelete: function(item){                	
		            $('#token-input-text-destino').prop('disabled', true)
                	$("#text-destino").tokenInput("clear");//hacer disabled
                }
			});
		
			$('#fechaIda').datepicker({
			    format: 'dd/mm/yyyy',
			    todayBtn: "linked",
			    language: "es",
			    todayHighlight: true,
			    startDate: new Date()
			})
				.on('changeDate', function (e) {
					$('#fechaVuelta').datepicker('setStartDate', e.date);
				});
			
			$('#fechaVuelta').datepicker({
			    format: 'dd/mm/yyyy',
			    language: "es",
			    todayHighlight: true,
			    startDate: new Date()
			});
			
		
    	});
		
		
		$(document).ready(function() {
	        $(".slide-1").loopItNow({
	             vertical:false,
	             effect:'',
	             captionTop:true,
	             captionBottom:true,
	             speed : 3000,

	            });
	        
	    	// add button style 
			$("[data-name='poll_bar'").addClass("btn btn-default");
			// Add button style with alignment to left with margin.
			$("[data-name='poll_bar'").css({"text-align":"left","margin":"1%"});		
			
			//loop 
			$("[data-name='poll_bar'").each(
					function(i){			
					  //get poll value 	
						var bar_width = (parseFloat($("[data-name='poll_val'").eq(i).text())/2).toString();					
						bar_width = bar_width + "%"; //add percentage sign.										
						//set bar button width as per poll value mention in span.
						$("[data-name='poll_bar'").eq(i).width(bar_width);					
						
						//Define rules.
						var bar_width_rule = parseFloat($("[data-name='poll_val'").eq(i).text());					 
						if(bar_width_rule >= 50){$("[data-name='poll_bar'").eq(i).addClass("btn btn-sm btn-success")}
						if(bar_width_rule <  50){$("[data-name='poll_bar'").eq(i).addClass("btn btn-sm btn-warning")}
						if(bar_width_rule <= 10){$("[data-name='poll_bar'").eq(i).addClass("btn btn-sm btn-danger")}					
						
						//Hide dril down divs
						$("#" + $("[data-name='poll_bar'").eq(i).text()).hide();
		  });
			
			//On click main menu bar show its particular detail div.
			  $("[data-name='poll_bar'").click(function()
			  {  
			     //Hide all 
			     $(".panel-body").children().hide();
			     //Display only selected bar texted div sub chart.
			     $("#" + $(this).text()).show();
			     //If not inner drill down sub detail found then move to main menu.
			     if($("#" + $(this).text()).length == 0) {
		 				   $("#Main").show();	         
						}				
			  }); 
	      
	    });
    </script>
    
  </body>
</html>