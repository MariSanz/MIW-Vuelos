<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security"%>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><spring:message code="titulo"/></title>

    <link href='<c:url value="/resources/bootstrap/css/bootstrap.min.css"/>' rel="stylesheet">
    <link rel="stylesheet" type="text/css" href='<c:url value="/resources/token-input/token-input.css"/>' />
    <link rel="stylesheet" type="text/css" href='<c:url value="/resources/css/loginStyle.css"/>' />
   	<!-- Estilos personalizados -->
   	<link href='<c:url value="/resources/css/style.css"/>' rel="stylesheet">
    <!-- Fuentes de Google -->
    <link href="https://fonts.googleapis.com/css?family=Fjalla+One" rel="stylesheet">
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
    
  </head>
<body>

	
  	  	<!--  Inicio NavBar -->
	  <nav class="navbar navbar-inverse">
	  <div class="container-fluid">
	    <!-- Banda para mostrarse en dispositivos moviles -->
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href='<c:url value="/"/>'><spring:message code="principal.nombre"/></a>
	    </div>
	
	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      <ul class="nav navbar-nav">
	        <li class="active"><a href='<c:url value="/"/>'><spring:message code="principal.buscar"/><span class="sr-only">(Actual)</span></a></li>
	        <security:authorize access="isAuthenticated()">
	     		<li><a href="<c:url value="/private/historialVuelos"/>"  role="button"><spring:message code="principal.historial"/></a></li>
	     	</security:authorize>
	     	<security:authorize access="!isAuthenticated()">
	     		<li><a href="cancelarReserva"  role="button"><spring:message code="principal.cancelar"/></a></li>
	     	</security:authorize>
	      </ul>
	     
	
	      <ul class="nav navbar-nav navbar-right">
	     	
	     	<security:authorize access="isAuthenticated()">
	     		<security:authentication var="principal" property="principal" />
	     		<li><a href="<c:url value="/"/>"  role="button"> ${principal.username} </a></li>
				<li>
					<a href='<c:url value="j_spring_security_logout"/>'  role="button"><spring:message code="login.logout"/></a>
				</li>
			</security:authorize>
			<security:authorize access="!isAuthenticated()">
				<li>
		      		<a href="<c:url value="login"/>"  role="button"><spring:message code="login_registro"/></a>
		      	</li>
		     </security:authorize>
		      	
		     
	       	 <li class="dropdown">
	          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
	          	<spring:message code="idioma"/>
	          	<span class="caret"></span>
	          </a>
	          <ul class="dropdown-menu">
	            <li><a href="?locale=es"><spring:message code="spanish"/></a></li>
	            <li><a href="?locale=en"><spring:message code="english"/></a></li>
	          </ul>
	        </li>
	        
	      </ul>
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>
  <!-- Fin NavBar -->
	<div class="container-fluid">
    	<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div class="panel panel-login">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-6">
								<a href="#" class="active" id="login-form-link">Login</a>
							</div>
							<div class="col-xs-6">
								<a href="#" id="register-form-link">Registro</a>
							</div>
						</div>
						<hr>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-12">
								<c:if test="${not empty error}">
									<div class="alert alert-danger">
									  <strong><spring:message code="principal.error"/></strong> <spring:message code="${error}"/> 
									</div>
								</c:if>	    
		
								<c:if test="${not empty message}">
									<div class="alert alert-info">
									  <strong><spring:message code="principal.informacion"/></strong>  <spring:message code="${message}"/>
									</div>
								</c:if>	
								<form id="login-form" name='loginForm'
									action="<c:url value='j_spring_security_check' />"  method="post" role="form" style="display: block;">
									<div class="form-group">
										<input type="text" name='j_username' value='' id="username" required="required" tabindex="1" class="form-control" placeholder="E-mail">
									</div>
									<div class="form-group">
										<input type="password"  name='j_password' id="password" required="required" tabindex="2" class="form-control" placeholder="Contrase�a">
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-sm-6 col-sm-offset-3">
												<input type="submit" name="login-submit" id="login-submit" tabindex="4" class="form-control btn btn-login" value="Iniciar sesi�n">
											</div>
										</div>
									</div>
									
								</form>
								<form:form id="register-form" action="registro"  commandName="registroForm" method="post" role="form" style="display: none;">
									<div class="form-group">
										<form:input type="text" path="usuario" required="required" id="username" tabindex="1" class="form-control" placeholder="Nombre" value=""></form:input>
										<form:errors path="usuario"/>
									</div>
									<div class="form-group">
										<form:input type="email" path="email" required="required" id="email" tabindex="1" class="form-control" placeholder="E-mail" value=""></form:input>
										<form:errors path="email"/>
									</div>
									<div class="form-group">
										<form:input type="password" path="pass" required="required" id="password" tabindex="2" class="form-control" placeholder="Contrase�a"></form:input>
										<form:errors path="pass"/>
									</div>
									<div class="form-group">
										<form:input type="password" path="repPass" required="required" id="confirm-password" tabindex="2" class="form-control" placeholder="Confirmar contrase�a"></form:input>
										<form:errors path="repPass"/>
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-sm-6 col-sm-offset-3">
												<input type="submit" name="register-submit" id="register-submit" tabindex="4" class="form-control btn btn-register" value="Registrarse ahora">
											</div>
										</div>
									</div>
								</form:form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	<!-- Pie de p�gina -->
	<div id="footer">
		<div class="row boton botton">
			<div class="col-md-3 col-xs-12 text-center">
				<span class="glyphicon glyphicon-copyright-mark"></span>
				<strong><spring:message code="principal.copyright"/></strong>
			</div>
			<div class="col-md-4 hidden-xs text-center">
				<strong><spring:message code="principal.alumna"/></strong>
			</div>
			<div class="col-md-4 hidden-xs text-center">
				<strong><spring:message code="principal.master"/></strong>
			</div>
		</div>


	</div>
	<!-- Fin pie de p�gina -->
	</div>
	
	
	<script src='<c:url value="/resources/jquery/jquery.js"/>' ></script>
	<script src='<c:url value="/resources/bootstrap/js/bootstrap.min.js"/>'></script>
	<script>
	$( function() {
	
		    $('#login-form-link').click(function(e) {
				$("#login-form").delay(100).fadeIn(100);
		 		$("#register-form").fadeOut(100);
				$('#register-form-link').removeClass('active');
				$(this).addClass('active');
				e.preventDefault();
			});
			$('#register-form-link').click(function(e) {
				$("#register-form").delay(100).fadeIn(100);
		 		$("#login-form").fadeOut(100);
				$('#login-form-link').removeClass('active');
				$(this).addClass('active');
				e.preventDefault();
			});
	
		});
	</script>
</body>
</html>
